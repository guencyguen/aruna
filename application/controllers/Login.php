<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 5/26/2018
 * Time: 12:11 AM
 */

class Login extends CI_Controller
{
    public function index() {
        if(!$this->session->has_userdata('id')
        && !$this->session->has_userdata('name')
        && !$this->session->has_userdata('username')) {
            $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
            $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
            $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
            $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
            $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);
            $this->load->view('auth/login', $data);
        } else redirect('dashboard');
    }

    public function proceed_login() {
        if($this->input->post('username') != NULL && $this->input->post('password') != NULL) {
            $employee = $this->db->get_where(
                'employee',
                array('username' => $this->input->post('username')),
                1
            )->result();

            if(sizeof($employee) > 0) {
                $employee = $employee[0];
                if($employee->username == $this->input->post('username') && password_verify($this->input->post('password'), $employee->password)) {
                    $this->session->set_userdata(array(
                        'id' => $employee->em_id,
                        'name' => $employee->em_name,
                        'username' => $employee->username,
                        'job_position_id' => $employee->job_position,
                        'job_position_name' => $this->db->get_where('job_position', array('id' => $employee->job_position), 1)->row()->name
                    ));
                    redirect('dashboard');
                } else {
                    $this->session->set_flashdata('invalid_login_message', 'Username atau kata sandi salah');
                    redirect('/');
                }
            } else {
                $this->session->set_flashdata('invalid_login_message', 'Username atau kata sandi salah');
                redirect('/');
            }
        } else {
            $this->session->set_flashdata('invalid_login_message', 'Username dan kata sandi harus diisi');
            redirect('/');
        }
    }
}