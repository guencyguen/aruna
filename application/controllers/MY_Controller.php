<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if(!$this->session->has_userdata('id') && !$this->session->has_userdata('name') && !$this->session->has_userdata('username'))
        {
            redirect('/login');
        }
        else
        {
            // Validation
            if($this->uri->segment(1) != null)
            {
                $status = $this->acl->allow($this->uri->segment(1));
                if($status == 'NOT ALLOWED') show_404();
            }
        }
    }
}
?>