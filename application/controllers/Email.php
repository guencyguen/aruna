<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'controllers'.DIRECTORY_SEPARATOR.'MY_Controller.php');

class Email extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $config = Array(
            'useragent' => 'CodeIgniter',
            'protocol'  => 'smtp',
            'mailpath'  => '/usr/sbin/sendmail',
            'smtp_host' => 'ssl://smtp.gmail.com',
            'smtp_user' => 'cs.aruna.id@gmail.com',
            'smtp_pass' => 'arunaadmin',
            'smtp_port' => 465,
            'smtp_keepalive' => TRUE,
            'smtp_crypto' => 'SSL',
            'wordwrap'  => TRUE,
            'wrapchars' => 80,
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
            'validate'  => TRUE,
            'crlf'      => "\r\n",
            'newline'   => "\r\n",
        );
        $this->load->library('email', $config);

        $this->email->from('cs.aruna.id@gmail.com', 'Aruna-CS');
    }

    public function welcomingMail($mail, $name)
    {
        $data['mail'] = base64_decode($mail);
        $data['name'] = base64_decode($name);

        $this->email->to($data['mail']);
        $this->email->subject('Kejutan Aruna Membership');
        $this->email->message($this->load->view('email/welcome', $data, TRUE));

        $this->email->send();
        redirect(site_url('pelanggan/index'));
    }

    public function birthdayMail()
    {
        $this->load->model('PelangganModel');
        $members = $this->PelangganModel->getBirthdayMember();
        $len = count($members);
        for ($i = 0; $i < $len; $i++) {
            $data['name'] = $members[$i]['mem_fullname'];
            $this->email->to($members[$i]['mem_email']);
            $this->email->subject('Kejutan Aruna Membership');
            $this->email->message($this->load->view('email/birthday', $data, TRUE));
            $this->email->send();
        }
    }
}