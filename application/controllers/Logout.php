<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller
{
    public function proceed_logout() {
        session_destroy();
        redirect('/');
    }
}
?>