<?php
/**
 * Created by Hauw.
 * User: anitut
 * Date: 6/26/2018
 * Time: 18:05 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'controllers'.DIRECTORY_SEPARATOR.'MY_Controller.php');

class Pembukuan extends MY_Controller
{
    private $filename = "import_data";
    public function __construct()
    {
        parent::__construct();
        $this->load->model('PembukuanModel');
    }

    public function pengeluaran()
    {
        $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
        $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
        $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
        $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
        $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);

        $data['addstock'] = $this->PembukuanModel->pengeluaran();
        $data['total'] = $this->PembukuanModel->total_pengeluaran();
        $this->load->view('pembukuan/pengeluaran', $data);
    }

    public function pemasukan()
    {
        $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
        $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
        $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
        $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
        $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);

        $data['transaction'] = $this->PembukuanModel->pemasukan();
        $data['total'] = $this->PembukuanModel->total_pemasukan();
        $this->load->view('pembukuan/pemasukan', $data);
    }

}