<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 5/31/2018
 * Time: 9:31 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'controllers'.DIRECTORY_SEPARATOR.'MY_Controller.php');

class Supplier extends MY_Controller
{
    private $filename = "import_data";
    public function __construct()
    {
        parent::__construct();
        $this->load->model('SupplierModel');
    }

    public function index()
    {
        $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
        $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
        $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
        $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
        $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);

        $data['suppliers'] = $this->SupplierModel->getAll();
        $this->load->view('supplier/listSupplier', $data);
    }

    public function addSupplier()
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST")
        {
            $data = Array (
                's_nama' => $this->input->post('nama'),
                's_email' => $this->input->post('email'),
                's_telp' => $this->input->post('notelp'),
                's_address' => $this->input->post('alamat'),
                's_city' => $this->input->post('kota'),
                's_province' => $this->input->post('provinsi'),
                's_country' => $this->input->post('negara'),
                's_postal_code' => $this->input->post('kodepos')
            );

            $this->SupplierModel->insert(html_escape($data), false);
            redirect(site_url("supplier/index"));
        }
        else
        {
            $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
            $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
            $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
            $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
            $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);

            $this->load->view('supplier/addSupplier', $data);
        }
    }

    public function edit($id)
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST")
        {
            $data = Array (
                's_id' => $id,
                's_nama' => $this->input->post('nama'),
                's_email' => $this->input->post('email'),
                's_telp' => $this->input->post('notelp'),
                's_address' => $this->input->post('alamat'),
                's_city' => $this->input->post('kota'),
                's_province' => $this->input->post('provinsi'),
                's_country' => $this->input->post('negara'),
                's_postal_code' => $this->input->post('kodepos')
            );

            $this->SupplierModel->update(html_escape($data), $id, false);
            redirect(site_url("supplier/index"));
        }
        else
        {
            $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
            $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
            $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
            $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
            $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);

            $data['supplier'] = $this->SupplierModel->getSpecified($id);

            $this->load->view('supplier/editSupplier', $data);
        }
    }

    public function detail($id)
    {
        $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
        $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
        $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
        $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
        $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);

        $data['supplier'] = $this->SupplierModel->getSpecified($id);

        $this->load->model('ProductModel');
        $data['products'] = $this->ProductModel->getProductFrom($id);

        $this->load->view('supplier/detailSupplier', $data);
    }

    public function delete($id)
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST") { //prevent XSS attack
            $this->SupplierModel->delete($id);
        }
        redirect(site_url('supplier/index'));
    }

    public function form()
    {
        include APPPATH.'third_party/PHPExcel/PHPExcel.php';
        $this->load->helper("file");

        $data['preview'] = false;
        $data['status'] = '';
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (!is_null($this->input->post('uploadfile'))) {
                $config['upload_path'] = 'excel/';
                $config['allowed_types'] = 'xlsx';
                $config['max_size'] = '2048';
                $config['overwrite'] = true;
                $config['file_name'] = $_FILES['files']['name'];
                $this->load->library('upload', $config);
                $filename = $_FILES['files']['name'];
                $valid =  (substr($filename, 0, 2) == 'S-') ? 1 : 0;

                if ($this->upload->do_upload('files') && $valid) {
                    $upload = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
                    $data['preview'] = true;
                    $filename = str_replace(" ", "_", strtolower($filename));
                    chmod('excel/'.$filename, 0777);
                    $excelreader = new PHPExcel_Reader_Excel2007();
                    $loadexcel = $excelreader->load('excel/' . $filename);
                    $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);
                    $data['sheet'] = $sheet;
                } else {
                    $upload = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
                    $data['status'] = ($valid == 0) ? "File tidak cocok." : $upload['error'];
                }
                $data['filename'] = $filename;
            }
            elseif (!is_null($this->input->post('preview'))) {
                $excelreader = new PHPExcel_Reader_Excel2007();
                $loadexcel = $excelreader->load('excel/'.$this->input->post('filename'));
                $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
                $data = [];
                foreach($sheet as $key => $row){
                    if ($key == 1) continue;
                    if ($row['A'] == '') continue;
                    array_push($data, [
                        's_nama' => $row['A'],
                        's_email' => $row['B'],
                        's_telp' => $row['C'],
                        's_address' => $row['D'],
                        's_city' => $row['E'],
                        's_province' => $row['F'],
                        's_country' => $row['G'],
                        's_postal_code' => $row['H']
                    ]);
                }
                $this->SupplierModel->insert_multiple($data);
                unlink("excel/".$this->input->post('filename'));
                redirect("supplier/index");
            }
        }
        $data['css'] = $this->load->view('include/style.php', null, true);
        $data['js'] = $this->load->view('include/script.php', null, true);
        $data['layout'] = $this->load->view('layout/layout.php', null, true);
        $data['footer'] = $this->load->view('layout/footer.php', null, true);
        $data['preloader'] = $this->load->view('layout/preloader.php', null, true);
        $this->load->view('supplier/form', $data);
    }
}