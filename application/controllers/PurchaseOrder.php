<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'controllers'.DIRECTORY_SEPARATOR.'MY_Controller.php');

class PurchaseOrder extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('AddStockModel');
    }

    public function index()
    {
        $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
        $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
        $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
        $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
        $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);

        $data['po'] = $this->AddStockModel->getAllPO();
        $this->load->view('purchaseorder/listPO', $data);
    }

    public function detailorder($id)
    {
        $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
        $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
        $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
        $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
        $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);

        $data['order'] = $this->AddStockModel->getSpecified($id);
        $data['items'] = $this->AddStockModel->getDetailWithSupplier($id);
        $data['po'] = $this->AddStockModel->getOrderPo($id);
        $this->load->view('purchaseorder/detailPraPO', $data);
    }

    public function detailPO($po)
    {
        $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
        $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
        $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
        $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
        $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);

        $this->load->model('SupplierModel');
        $data['order'] = $this->AddStockModel->getDetailPO($po);
        $data['supplier'] = $this->SupplierModel->getSpecified($data['order'][0]['s_id']);
        $data['em_position'] = $this->AddStockModel->getEmployeePos($data['order'][0]['em_id']);
        $this->load->view('purchaseorder/detailPO', $data);
    }

    public function generatePo($o_id, $s_id)
    {
        $this->load->model('ProductModel');
        $items = $this->AddStockModel->getFromSupplier($o_id, $s_id);

        // generate PO id
        $id = substr($o_id, 3, 7);
        $po = 'AR-PO-' . $s_id . '-' . $id;

        $len = count($items);
        for ($i = 0; $i < $len; $i++) {
            $data = Array (
                'po' => $po,
                'po_date' => date('Y-m-d')
            );
            $this->AddStockModel->updatePO(html_escape($data), $o_id, $s_id, false);

            $this->ProductModel->addStok($items[$i]['p_id'], $items[$i]['qty']);
        }
        redirect(site_url("purchaseorder/detailorder/".$o_id));
    }

    public function cancelPo($po)
    {
        $o_id = $this->AddStockModel->getDetailPO($po);
        $this->AddStockModel->cancelPO($po);

        $len = count($o_id);
        $this->load->model('ProductModel');
        for ($i = 0; $i < $len; $i++) {
            $this->ProductModel->minusStok($o_id[$i]['p_id'], $o_id[$i]['qty']);
        }
        redirect(site_url("purchaseorder/detailorder/".$o_id[0]['o_id']));
    }

    public function downloadPo($po)
    {
        $this->load->model('SupplierModel');
        $data['order'] = $this->AddStockModel->getDetailPO($po);
        $data['supplier'] = $this->SupplierModel->getSpecified($data['order'][0]['s_id']);

        $this->load->library('pdf');
        $this->pdf->load_view('purchaseorder/purchaseorder', $data);
        $this->pdf->set_paper('A4', 'portrait');
        $this->pdf->render();
        $this->pdf->stream($po.".pdf", array("Attachment" => false));
    }
}