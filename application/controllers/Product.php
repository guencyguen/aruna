<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 5/31/2018
 * Time: 9:31 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'controllers'.DIRECTORY_SEPARATOR.'MY_Controller.php');

class Product extends MY_Controller
{
//    private $filename = "import_data";
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ProductModel');
    }

    public function index()
    {
        $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
        $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
        $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
        $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
        $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);

        $data['products'] = $this->ProductModel->getAll();
        $data['product'] = $this->ProductModel->view();
        $this->load->view('barang/listBarang', $data);
    }

    public function addProduct()
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST")
        {
            $data = Array (
                'pr_name' => $this->input->post('nama'),
                'pr_category' => $this->input->post('kategori'),
                'harga_jual' => $this->input->post('harga_jual'),
                'harga_beli' => $this->input->post('harga_beli'),
                'pr_stock' => $this->input->post('stok_toko'),
                'pr_inventory' => $this->input->post('stok_gudang'),
                's_id' => $this->input->post('supplier')
            );

            $this->ProductModel->insert(html_escape($data), false);
            redirect(site_url("product/index"));
        }
        else
        {
            $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
            $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
            $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
            $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
            $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);

            $this->load->model('SupplierModel');
            $data['suppliers'] = $this->SupplierModel->getAll();
            $this->load->view('barang/addBarang', $data);
        }
    }

    public function edit($id)
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST")
        {
            $data = Array (
                'pr_id' => $id,
                'pr_name' => $this->input->post('nama'),
                'pr_category' => $this->input->post('kategori'),
                'harga_jual' => $this->input->post('harga_jual'),
                'harga_beli' => $this->input->post('harga_beli'),
                'pr_stock' => $this->input->post('stok_toko'),
                'pr_inventory' => $this->input->post('stok_gudang'),
                's_id' => $this->input->post('supplier')
            );

            $this->ProductModel->update(html_escape($data), $id, false);
            redirect(site_url("product/index"));
        }
        else
        {
            $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
            $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
            $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
            $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
            $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);

            $data['product'] = $this->ProductModel->getSpecified($id);

            $this->load->model('SupplierModel');
            $data['suppliers'] = $this->SupplierModel->getAll();
            $this->load->view('barang/editBarang', $data);
        }
    }

    public function restock()
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST")
        {
            $len = count($this->input->post('barang'));
            for ($i = 0; $i < $len; $i++) {
                if ($this->input->post('qty')[$i] == 0) continue;

                $product = $this->ProductModel->getSpecified($this->input->post('barang')[$i]);
                $data = Array (
                    'pr_inventory' => $product['pr_inventory'] - $this->input->post('qty')[$i],
                    'pr_stock' => $product['pr_stock'] + $this->input->post('qty')[$i]
                );
                $this->ProductModel->update(html_escape($data), $this->input->post('barang')[$i], false);
            }
            redirect(site_url("product/index"));
        }
        else
        {
            $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
            $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
            $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
            $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
            $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);

            $data['products'] = $this->ProductModel->getAllInventory();
            $this->load->view('barang/restock', $data);
        }
    }

    public function detail($id)
    {
        $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
        $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
        $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
        $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
        $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);

        $data['product'] = $this->ProductModel->getSpecified($id);

        $this->load->model('SupplierModel');
        $data['supplier'] = $this->SupplierModel->getSpecified($data['product']['s_id']);
        $this->load->view('barang/detailBarang', $data);
    }

    public function delete($id)
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST") { //prevent XSS attack
            $this->ProductModel->delete($id);
        }
        redirect(site_url('product/index'));
    }

    public function form()
    {
        include APPPATH.'third_party/PHPExcel/PHPExcel.php';
        $this->load->helper("file");

        $data['preview'] = false;
        $data['status'] = '';
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (!is_null($this->input->post('uploadfile'))) {
                $config['upload_path'] = 'excel/';
                $config['allowed_types'] = 'xlsx';
                $config['max_size'] = '2048';
                $config['file_name'] = $_FILES['files']['name'];
                $this->load->library('upload', $config);
                $filename = $_FILES['files']['name'];
                $valid =  (substr($filename, 0, 2) == 'P-') ? 1 : 0;

                if ($this->upload->do_upload('files') && $valid) {
                    $upload = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
                    $data['preview'] = true;
                    $filename = str_replace(" ", "_", strtolower($filename));
                    chmod('excel/'.$filename, 0777);
                    $excelreader = new PHPExcel_Reader_Excel2007();
                    $loadexcel = $excelreader->load('excel/' . $filename);
                    $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);
                    $data['status'] = '';
                    $data['sheet'] = $sheet;
                } else {
                    $upload = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
                    $data['status'] = ($valid == 0) ? "File tidak cocok." : $upload['error'];
                }
                $data['filename'] = $filename;
            }
            elseif (!is_null($this->input->post('preview'))) {
                $excelreader = new PHPExcel_Reader_Excel2007();
                $loadexcel = $excelreader->load('excel/'.$this->input->post('filename'));
                $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
                $data = [];
                foreach($sheet as $key => $row){
                    if ($key == 1) continue;
                    if ($row['A'] == '') continue;
                    array_push($data, [
                        'pr_name' => $row['A'],
                        'pr_stock' => 0,
                        'pr_inventory' => $row['B'],
                        'pr_category' => $row['C'],
                        'harga_jual' => $row['D'],
                        'harga_beli' => $row['E']
                    ]);
                }
                $this->ProductModel->insert_multiple($data);
                unlink("excel/".$this->input->post('filename'));
                redirect("product/index");
            }
        }
        $data['css'] = $this->load->view('include/style.php', null, true);
        $data['js'] = $this->load->view('include/script.php', null, true);
        $data['layout'] = $this->load->view('layout/layout.php', null, true);
        $data['footer'] = $this->load->view('layout/footer.php', null, true);
        $data['preloader'] = $this->load->view('layout/preloader.php', null, true);
        $this->load->view('barang/form', $data);
    }
}