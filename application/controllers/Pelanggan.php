<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'controllers'.DIRECTORY_SEPARATOR.'MY_Controller.php');

class Pelanggan extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('PelangganModel');
    }

    public function index()
    {
        $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
        $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
        $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
        $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
        $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);

        $data['members'] = $this->PelangganModel->getAllWithoutNN();
        $this->load->view('pelanggan/listPelanggan', $data);
    }

    public function addPelanggan()
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST")
        {
            $data = Array (
                'mem_fullname' => $this->input->post('nama'),
                'mem_address' => $this->input->post('alamat'),
                'mem_date_birth' => $this->input->post('tanggalLahir'),
                'mem_gender' => $this->input->post('gender'),
                'mem_email' => $this->input->post('email'),
                'poin' => $this->input->post('poin'),
            );

            $this->PelangganModel->insert(html_escape($data), false);
            redirect(site_url("email/welcomingMail/".base64_encode($this->input->post('email')).'/'.base64_encode($this->input->post('nama'))));
        }
        else
        {
            $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
            $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
            $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
            $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
            $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);

            $this->load->view('pelanggan/addPelanggan', $data);
        }
    }

    public function edit($id)
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST")
        {
            $data = Array (
                'mem_fullname' => $this->input->post('nama'),
                'mem_address' => $this->input->post('alamat'),
                'mem_date_birth' => $this->input->post('tanggalLahir'),
                'mem_gender' => $this->input->post('gender'),
                'mem_email' => $this->input->post('email'),
                'poin' => $this->input->post('poin'),
            );

            $this->PelangganModel->update(html_escape($data), $id, false);
            redirect(site_url("pelanggan/index"));
        }
        else
        {
            $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
            $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
            $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
            $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
            $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);

            $data['member'] = $this->PelangganModel->getSpecified($id);
            $date = date_create($data['member']['mem_date_birth']);
            $data['tanggal'] = date_format($date, "Y-m-d");

            $this->load->view('pelanggan/editPelanggan', $data);
        }
    }

    public function detail($id)
    {
        $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
        $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
        $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
        $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
        $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);

        $data['member'] = $this->PelangganModel->getSpecified($id);
        $date = date_create($data['member']['mem_date_birth']);
        $data['tanggal'] = date_format($date, "d M Y");

        $this->load->view('pelanggan/detailPelanggan', $data);
    }

    public function delete($id)
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST") { //prevent XSS attack
            $this->PelangganModel->delete($id);
        }
        redirect(site_url('pelanggan/index'));
    }
}