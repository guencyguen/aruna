<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 6/16/2018
 * Time: 1:24 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'controllers'.DIRECTORY_SEPARATOR.'MY_Controller.php');

class OrderBarang extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('AddStockModel');
    }

    public function index()
    {
        $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
        $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
        $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
        $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
        $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);

        $data['stocks'] = $this->AddStockModel->getAll();
        $this->load->view('orderbarang/listOrder', $data);
    }

    public function addStock()
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST")
        {
            $len = count($this->input->post('barang'));
            $this->load->model('ProductModel');

            // generate order id
            $o_id = $this->AddStockModel->getOrderId();
            if (is_null($o_id[0]['o_id'])) $id = '0001';
            else $id = str_pad(substr($o_id[0]['o_id'], 9, 4) + 1, 4, "0", STR_PAD_LEFT);

            for ($i = 0; $i < $len; $i++) {
                if ($this->input->post('qty')[$i] == 0) continue; //escape 0 qty

                $product = $this->ProductModel->getSpecified($this->input->post('barang')[$i]);
                $data = Array (
                    'o_id' => 'AR-OR-' . $id,
                    'em_id' => $this->input->post('em_id'),
                    'p_id' => $this->input->post('barang')[$i],
                    's_id' => $product['s_id'],
                    'qty' => $this->input->post('qty')[$i],
                    'price' => $product['harga_beli'],
                    'total_price' => $this->input->post('qty')[$i] * $product['harga_beli'],
                    'o_date' => date('Y-m-d')
                );

                $this->AddStockModel->insert(html_escape($data), false);
            }
            redirect(site_url("orderbarang/index"));
        }
        else
        {
            $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
            $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
            $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
            $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
            $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);

            $this->load->model('ProductModel');
            $data['products'] = $this->ProductModel->getAllWithSupplier();
            $this->load->view('orderbarang/addOrder', $data);
        }
    }

    public function edit($id)
    {
        $this->load->model('ProductModel');
        if ($_SERVER['REQUEST_METHOD'] == "POST")
        {
            $order = $this->AddStockModel->getSpecifiedWithoutPO($id);
            $len = count($order);

            for ($i = 0; $i < $len; $i++) {
                if ($this->input->post('qty')[$i] == 0) {
                    $this->AddStockModel->deleteItem($id, $this->input->post('barang')[$i]);
                    continue;
                }
                $idDb = $order[$i]['id'];

                $product = $this->ProductModel->getSpecified($this->input->post('barang')[$i]);
                $data = Array (
                    'p_id' => $this->input->post('barang')[$i],
                    's_id' => $product['s_id'],
                    'qty' => $this->input->post('qty')[$i],
                    'price' => $product['harga_beli'],
                    'total_price' => $this->input->post('qty')[$i] * $product['harga_beli']
                );
                $this->AddStockModel->update(html_escape($data), $idDb, false);
            }
            redirect(site_url("orderbarang/index"));
        }
        else
        {
            $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
            $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
            $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
            $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
            $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);

            $data['order'] = $this->AddStockModel->getSpecifiedWithoutPO($id);
            $data['products'] = $this->ProductModel->getAllWithSupplier();
            $this->load->view('orderbarang/editOrder', $data);
        }
    }

    public function detail($id)
    {
        $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
        $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
        $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
        $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
        $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);

        $data['order'] = $this->AddStockModel->getSpecified($id);
        $data['items'] = $this->AddStockModel->getDetailWithSupplier($id);
        $this->load->view('orderbarang/detailOrder', $data);
    }

    public function delete($id)
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST") { //prevent XSS attack
            $this->AddStockModel->delete($id);
        }
        redirect(site_url('orderbarang/index'));
    }

    public function deleteItem($id)
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST") { //prevent XSS attack
            $this->AddStockModel->deleteFromId($id);
        }
        redirect(site_url('orderbarang/index'));
    }
}