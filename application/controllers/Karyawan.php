<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'controllers'.DIRECTORY_SEPARATOR.'MY_Controller.php');

class Karyawan extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('KaryawanModel');
        $this->load->model('JabatanModel');
    }

    public function index()
    {
        $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
        $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
        $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
        $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
        $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);

        $data['karyawan'] = $this->KaryawanModel->getAll();
        $data['jabatan'] = array();
        $job_positions = $this->JabatanModel->getAll();
        foreach($job_positions as $job_position)
        {
            $data['jabatan'][$job_position['id']] = $job_position['name'];
        }

        $this->load->view('karyawan/listKaryawan', $data);
    }

    public function addKaryawan()
    {
        $this->load->library('form_validation');

        $data = array(
            'em_name' => $this->input->post('nama'),
            'em_address' => $this->input->post('alamat'),
            'date_birth' => $this->input->post('ttl'),
            'em_gender' => $this->input->post('gender'),
            'em_email' => $this->input->post('email'),
            'username' => $this->input->post('username'),
            'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT),
            'em_phone_number' => $this->input->post('no_telp'),
            'job_position' => $this->input->post('posisi'),
            'date_entry' => date('Y-m-d')
        );

        $this->form_validation->set_rules(array(
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'is_unique[employee.em_email]',
                'errors' => array(
                    'is_unique' => '%s sudah terdaftar'
                )
            ),
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'is_unique[employee.username]',
                'errors' => array(
                    'is_unique' => '%s sudah terdaftar'
                )
            )
        ));

        if($this->form_validation->run())
        {
            $this->KaryawanModel->insert(html_escape($data));
            redirect(site_url("karyawan/index"));
        }
        else
        {
            $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
            $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
            $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
            $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
            $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);

            $this->load->view('karyawan/addKaryawan', $data);
        }
    }

    public function edit($id)
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST")
        {
            $data = array(
                'em_name' => $this->input->post('nama'),
                'date_birth' => $this->input->post('tanggalLahir'),
                'em_gender' => $this->input->post('gender'),
                'date_entry' => $this->input->post('tanggalMulaiKerja'),
                'job_position' => $this->input->post('jabatan'),
                'em_address' => $this->input->post('alamat'),
                'em_email' => $this->input->post('email'),
                'em_phone_number' => $this->input->post('telepon')
            );

            $this->KaryawanModel->update(html_escape($data), $id, false);
            redirect(site_url("karyawan/index"));
        }
        else
        {
            $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
            $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
            $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
            $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
            $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);

            $data['karyawan'] = $this->KaryawanModel->getSpecified($id);
            $date = date_create($data['karyawan']['date_birth']);
            $data['karyawan']['date_birth'] = date_format($date, "Y-m-d");
            $date = date_create($data['karyawan']['date_entry']);
            $data['karyawan']['date_entry'] = date_format($date, "Y-m-d");

            $data['jabatan'] = $this->JabatanModel->getAll();

            $this->load->view('karyawan/editKaryawan', $data);
        }
    }

    public function detail($id)
    {
        $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
        $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
        $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
        $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
        $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);

        $data['karyawan'] = $this->KaryawanModel->getSpecified($id);
        $date = date_create($data['karyawan']['date_entry']);
        $data['karyawan']['tanggal_masuk'] = date_format($date, "d M Y");
        $data['jabatan'] = array();
        $job_positions = $this->JabatanModel->getAll();
        foreach($job_positions as $job_position)
        {
            $data['jabatan'][$job_position['id']] = $job_position['name'];
        }

        $this->load->view('karyawan/detailKaryawan', $data);
    }

    public function delete($id)
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST") { //prevent XSS attack
            $this->KaryawanModel->delete($id);
        }
        redirect(site_url('karyawan/index'));
    }
}
?>