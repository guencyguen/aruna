<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'controllers'.DIRECTORY_SEPARATOR.'MY_Controller.php');

class Penjualan extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('PenjualanModel');
    }

    public function menuKasir(){
        if ($_SERVER['REQUEST_METHOD'] == "POST")
        {
            $total_price = 0;
            $total_profit = 0;
            $len = count($this->input->post('barang'));
            $disc = $this->input->post('diskon'); 
            
            //generate id transaksi
            $t_id = date_format(date_create(date('Y/m/d H:i:s')), "ymdHis");

            $data_transaction = Array (
                't_id' => $t_id,
                'date_transaction' => date_format(date_create(date('Y/m/d H:i:s')), "Y-m-d H:i:s"),
                'mem_id' => $this->input->post('id_pelanggan'),
                'total_price' => '0',
                'total_profit' => '0',
                'em_id' => $this->session->userdata('id'),
                'disc' => $disc,
            );
            $this->PenjualanModel->insert_transaction(html_escape($data_transaction), false);

            for ($i = 0; $i < $len; $i++) {
                if ($this->input->post('qty')[$i] == 0) continue; //escape 0 qty

                $data['product_id'] = $this->PenjualanModel->getSpecifiedProduct($this->input->post('barang')[$i]);

                $qty = $this->input->post('qty')[$i];


                $price = (($data['product_id']['harga_jual']) * $qty);

                $profit = ($price - (($data['product_id']['harga_beli']) * $qty));

                $total_price = $total_price + $price;
                $total_profit = $total_profit + $profit;

                $data_product_sale = Array (
                    'pr_id' => $this->input->post('barang')[$i],
                    't_id' => $t_id,
                    'qty' => $qty,
                    'price' => $price,
                    'profit' => $profit,
                );
                $this->PenjualanModel->insert_product_sale(html_escape($data_product_sale), false);

                $stock = ($data['product_id']['pr_stock'] - $qty);
                $data_product = Array(
                    'pr_stock' => $stock,
                );
                $this->PenjualanModel->updateProduct(html_escape($data_product), $this->input->post('barang')[$i], false);
            }

            $total_price = $total_price - (($total_price * $disc) / 100);
            $total_profit = $total_profit - (($total_profit * $disc) / 100);
            $data_transaction = Array (
                'total_price' => $total_price,
                'total_profit' => $total_profit,
            );
            $this->PenjualanModel->update_transaction(html_escape($data_transaction), $t_id, false);

            $data['member'] = $this->PenjualanModel->getSpecifiedMember($this->input->post('id_pelanggan'));

            $poin = intval($total_price / 1000);
            $poins = ($data['member']['poin'] + $poin);

            $data_member = Array (
                'mem_fullname' => $data['member']['mem_fullname'],
                'mem_address' => $data['member']['mem_address'],
                'mem_date_birth' => $data['member']['mem_date_birth'],
                'mem_gender' => $data['member']['mem_gender'],
                'mem_email' => $data['member']['mem_email'],
                'poin' => $poins,
            );

            $data['tanggal'] = date_format(date_create(date('Y/m/d H:i:s')), "d M Y H:i:s");
            $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
            $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
            $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
            $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
            $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);
            $data['product'] = $this->PenjualanModel->getAllProduct();
            $data['product_sale'] = $this->PenjualanModel->getListProductSale($t_id);
            $data['transaction'] = $this->PenjualanModel->getListTransaction($t_id);
            
            $this->PenjualanModel->updateMember(html_escape($data_member), $data['transaction']['mem_id'], false);

            $this->load->view('penjualan/transaksi', $data);
        }
        else
        {
            $data['tanggal'] = date_format(date_create(date('Y/m/d H:i:s')), "d M Y H:i:s");
            $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
            $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
            $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
            $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
            $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);
            $data['member'] = $this->PenjualanModel->getAllMember();
            $data['product'] = $this->PenjualanModel->getAllProduct();

            $this->load->view('penjualan/menuKasir_', $data);
        }
    }

    public function addTransaksi($t_id)
    {
        $data['transaksi'] = TRUE;
        $data['tanggal'] = date_format(date_create(date('Y/m/d H:i:s')), "d M Y H:i:s");
        $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
        $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
        $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
        $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
        $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);
        $data['product'] = $this->PenjualanModel->getAllProduct();
        $data['product_sale'] = $this->PenjualanModel->getListProductSale($t_id);
        $data['transaction'] = $this->PenjualanModel->getListTransaction($t_id);

        $data['member'] = $this->PenjualanModel->getSpecifiedMember($data['transaction']['mem_id']);

        $poin = intval($data['transaction']['total_price'] / 1000);
        $poins = ($data['member']['poin'] + $poin);

        $data_member = Array (
            'mem_fullname' => $data['member']['mem_fullname'],
            'mem_address' => $data['member']['mem_address'],
            'mem_date_birth' => $data['member']['mem_date_birth'],
            'mem_gender' => $data['member']['mem_gender'],
            'mem_email' => $data['member']['mem_email'],
            'poin' => $poins,
        );
        
        $this->PenjualanModel->updateMember(html_escape($data_member), $data['transaction']['mem_id'], false);

        $this->load->view('penjualan/transaksi', $data);
    }

    public function daftarTransaksi()
    {
        $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
        $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
        $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
        $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
        $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);
        $data['product'] = $this->PenjualanModel->getAllProduct();
        $data['transaction'] = $this->PenjualanModel->getAllTransaction();      
        
        $this->load->view('penjualan/daftarTransaksiPenjualan', $data);
    }

    public function detailTransaksi($t_id)
    {
        
        $data['css'] = $this->load->view('include/style.php', NULL, TRUE);
        $data['js'] = $this->load->view('include/script.php', NULL, TRUE);
        $data['layout'] = $this->load->view('layout/layout.php', NULL, TRUE);
        $data['footer'] = $this->load->view('layout/footer.php', NULL, TRUE);
        $data['preloader'] = $this->load->view('layout/preloader.php', NULL, TRUE);
        $data['product_sale'] = $this->PenjualanModel->getListProductSale($t_id);
        $data['transaction'] = $this->PenjualanModel->getListTransaction($t_id);
        $data['tanggal'] = date_format(date_create($data['transaction']['date_transaction']), "d M Y H:i:s");

        $this->load->view('penjualan/detailTransaksi', $data);
    }
}