<html>
<head>
    <title>Kejutan Aruna</title>
</head>
<body>
    <div>
        <p><strong>Halo, <?php echo $name; ?>!</strong></p>
        <p>
            Terima kasih telah bergabung bersama Aruna.<br/>
            Kami sangat senang menyambut Anda dan kami suka memberikan kejutan. Untuk itu, <b>Anda berhak mendapatkan potongan belanja sebesar 10%!</b>
            Anda hanya perlu menunjukkan email ini pada saat melakukan pembayaran di kasir. Kejutan ini dapat Anda nikmati sampai dengan 5 hari ke depan.
        </p>
        <p>
            Apabila terdapat pertanyaan, silakan kunjungi bagian Customer Service kami. Anda juga dapat menghubungi kami via telpon ke 021-363688 atau email ke cs.aruna.id@gmail.com.
        </p>
        <br>
        <p>
            Kejutan dari Aruna tidak berhenti sampai di sini, lho. Pastikan Anda tidak melewatkan email kejutan dari Aruna! :)
        </p>
        <br>
        <p>
            Salam hangat,<br>
            Aruna<br>
        </p>
        <br>
        <hr>
        <p><i>Dikirim dengan Aruna Supermarket Management System</i></p>
    </div>
</body>
</html>