<html>
<head>
    <title>Kejutan Aruna</title>
</head>
<body>
    <div>
        <p><strong>Happy Birthday, <?php echo $name; ?>!</strong></p>
        <p>
            Semoga Anda senantiasa sehat dan bahagia, ya!<br/>
            Kami turut berbahagia pada hari ini dan kami suka memberikan kejutan. Untuk merayakannya, <b>Anda berhak mendapatkan potongan belanja sebesar 15%!</b>
            Anda hanya perlu menunjukkan email ini pada saat melakukan pembayaran di kasir. Yuk, rayakan ulang tahun Anda hari ini di Supermarket Aruna! Kejutan ini berlaku untuk hari ini saja.
        </p>
        <p>
            Apabila terdapat pertanyaan, silakan kunjungi bagian Customer Service kami. Anda juga dapat menghubungi kami via telpon ke 021-363688 atau email ke cs.aruna.id@gmail.com.
        </p>
        <br>
        <p>
            Kejutan dari Aruna tidak berhenti sampai di sini, lho. Pastikan Anda tidak melewatkan email kejutan dari Aruna! :)<br>
            Sekali lagi, happy birthday, <?php echo $name; ?>! Wish you all the best!
        </p>
        <br>
        <p>
            Salam hangat,<br>
            Aruna<br>
        </p>
        <br>
        <hr>
        <p><i>Spesial untuk Anda, dikirim dengan Aruna Supermarket Management System</i></p>
    </div>
</body>
</html>