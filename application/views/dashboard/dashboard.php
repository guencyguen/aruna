<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 5/26/2018
 * Time: 2:11 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ARUNA</title>
    <?php echo $css; ?>
</head>
<body class="fix-header fix-sidebar">
    <?php echo $preloader; ?>

    <div id="main-wrapper">
        <?php echo $layout;?>

        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text">Dashboard</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Beranda</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluid  -->
            <div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <?php if($this->acl->allow('lihat_total_laba_bersih') == 'ALLOWED'): ?>
                        <div class="col-md-4">
                            <div class="card p-30">
                                <div class="media">
                                    <div class="media-left meida media-middle">
                                        <span><i class="fa fa-usd f-s-40 color-primary"></i></span>
                                    </div>
                                    <div class="media-body media-text-right">
                                        <h2>Rp <?php echo number_format($total_profit, 0); ?></h2>
                                        <p class="m-b-0">Total Laba Bersih</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($this->acl->allow('lihat_jumlah_transaksi_penjualan') == 'ALLOWED'): ?>
                        <div class="col-md-4">
                            <div class="card p-30">
                                <div class="media">
                                    <div class="media-left meida media-middle">
                                        <span><i class="fa fa-shopping-cart f-s-40 color-success"></i></span>
                                    </div>
                                    <div class="media-body media-text-right">
                                        <h2><?php echo $transaction_count; ?></h2>
                                        <p class="m-b-0">Jumlah Transaksi Penjualan</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($this->acl->allow('lihat_jumlah_po') == 'ALLOWED'): ?>
                        <div class="col-md-4">
                            <div class="card p-30">
                                <div class="media">
                                    <div class="media-left meida media-middle">
                                        <span><i class="fa fa-archive f-s-40 color-warning"></i></span>
                                    </div>
                                    <div class="media-body media-text-right">
                                        <h2><?php echo $po_count; ?></h2>
                                        <p class="m-b-0">Jumlah Purchase Order</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                <!-- End Page Content -->
            </div>
            <!-- End Container fluid  -->

            <?php echo $footer; ?>
        </div>

    </div>

</body>
<?php echo $js; ?>
</html>

<!--
CATATAN:
Bagian isi halaman terletak di dalam container-fluid.
Bagian breadcrumb itu disesuaikan dengan menunya.
Cara 101:
 - Liat di template, mau bikin menu dengan tampilan seperti apa
 - Copy paste bagian container-fluid
 - Hapus bagian yang tidak diperlukan
 - Apabila ada script JS yang belum di-include, silakan include sendiri dan masukkan ke dalam folder yang sesuai
-->