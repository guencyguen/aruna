<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $order[0]['po'] ?></title>
</head><body>

<header>
    <div id="invoice-top" style="height: 120px;">
        <div class="clearfix">
            <div style="padding-right: 55%; display: inline;">
                <img src="<?php echo base_url('/assets/icons/aruna2.png') ?>" width="100px" height="100px" />
            </div>
            <div style="display: inline-block;">
                <h4 style="margin-bottom: 0"><strong>PURCHASE ORDER</strong></h4>
                <p style="font-size: 13px; margin-top: 0"> Nomor: <?php echo $order[0]['po']; ?><br>
                    Tanggal PO: <?php echo date_format(new DateTime($order[0]['po_date']), 'd M Y'); ?><br>
                    Mata Uang: Rupiah
                </p>
            </div>
        </div>
    </div>
    <hr>
    <div id="invoice-mid" style="height: 350px;">
        <div class="invoice-info fleft" style="width: 45%">
            <p>Supplier:</p>
            <h6><strong><?php echo $supplier['s_nama']; ?></strong></h6>
            <h6><?php echo $supplier['s_address']; ?><br>
                <?php echo $supplier['s_city']; ?>, <?php echo $supplier['s_province']; ?>, <?php echo $supplier['s_country']; ?> - <?php echo $supplier['s_postal_code']; ?><br>
                <?php echo $supplier['s_telp']; ?>
            </h6>
        </div>
        <div class="fleft" style="width: 5%"></div>
        <div id="project" class="fleft" style="width: 45%">
            <p>Alamat Pengiriman:</p>
            <h6><strong>Rian Ananda Putra</strong></h6>
            <h6>Kepala Divisi Warehouse<br>
                Jln. Scientia Boulevard Gading Serpong<br>
                Tangerang Selatan, Banten, Indonesia - 15811<br>
                021-363688
            </h6>
        </div>
    </div>
</header>
<main>
    <div id="invoice-bot">
        <div id="invoice-table">
            <div class="table-responsive">
                <table class="table" width="100%">
                    <tr class="tabletitle">
                        <th class="table-item">
                            <h2>Nama Barang</h2>
                        </th>
                        <th>
                            <h2>Jumlah</h2>
                        </th>
                        <th>
                            <h2>Harga Satuan</h2>
                        </th>
                        <th>
                            <h2>Total Harga</h2>
                        </th>
                    </tr>

                    <?php
                    $sum = 0;
                    foreach ($order as $item) {
                        echo '<tr class="service">';
                        echo '<td class="tableitem"><p class="itemtext">'.$item['pr_name'].'</p></td>';
                        echo '<td class="tableitem"><p class="itemtext" style="text-align: center">'.$item['qty'].' unit</p></td>';
                        echo '<td class="tableitem"><p class="itemtext" style="text-align: right">'.number_format($item['price'], 0).'</p></td>';
                        echo '<td class="tableitem"><p class="itemtext">'.number_format($item['total_price'], 0).'</p></td>';
                        echo '</tr>';
                        $sum += $item['total_price'];
                    }
                    ?>

                    <tr class="tabletitle">
                        <td></td>
                        <td></td>
                        <td class="Rate">
                            <h2 style="text-align: right">Total Rp.</h2>
                        </td>
                        <td class="payment">
                            <h2 style="text-align: right"><?php echo number_format($sum, 0); ?></h2>
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>
</main>
<footer>
    <hr>
    <div id="legalcopy">
        <p class="legal">
            <strong>Terima kasih telah berbisnis bersama kami!</strong>
            Kami berharap pemesanan barang melalui PO ini dapat segera diproses.<br>
            Tenggat waktu pengiriman adalah 30 hari setelah tanggal PO.<br>
        </p>
        <p style="text-align: right">
            <i>Dibuat dengan Aruna Supermarket Management System.</i>
        </p>
    </div>
</footer>


<style>
    @page {
        margin-top: 350px;
        margin-bottom: 100px;
    }

    * {
        outline: none;
    }

    body {
        background: #fff;
        font-family: 'Open Sans', sans-serif;
        margin: 0;
        overflow-x: hidden;
        color: #67757c;
    }

    header {
        position: fixed;
        top: -300px;
    }

    footer {
        position: fixed;
        top: 650px;
    }

    main {
        position: relative;
        top: 20px;
    }

    html {
        position: relative;
        min-height: 100%;
        background: #ffffff;
    }

    div {
        display: block;
    }

    img {
        vertical-align: middle;
        border-style: none;
        float: left;
    }

    p {
        font-family: 'Poppins', sans-serif;
        color: #99abb4;
        display: block;
    }

    h1, h2, h3, h4, h5, h6 {
        color: #455a64;
        font-weight: 400;
    }

    h2 {
        font-size: 24px;
    }

    h4 {
        font-size: 18px;
    }

    h6 {
        font-size: 14px;
        margin-top: 0;
        margin-bottom: 5px;
    }

    hr {
        margin-bottom: 1rem;
        border: 0;
        border-top: 1px solid;
        border-color: lightgrey;
        box-sizing: content-box;
        height: 0;
        overflow: visible;
        display: block;
    }

    table {
        border-collapse: collapse;
        display: table;
        border-spacing: 1px;
        border-color: grey;
    }

    tbody tr td {
        font-family: 'Poppins', sans-serif;
        color: #99abb4;
    }

    tbody tr td:last-child {
        text-align: right;
    }

    td, th {
        vertical-align: inherit;
    }

    tbody {
        vertical-align: middle;
        border-color: inherit;
    }

    tr {
        vertical-align: inherit;
        border-color: inherit;
    }

    .clearfix::after{
        display:block;
        clear:both;
        content:""
    }

    .fleft {
        float: left;
        display: inline-block;
    }

    .fright {
        float: right;
        display: inline-block;
    }

    .title {
        float: right;
        display: inline-block;
    }

    .title h4 {
        color: #455a64;
        text-align: right;
    }

    .title p {
        text-align: right;
        font-size: 12px;
    }

    .invoice-info {
        margin-left: 20px;
    }

    .invoice-info p {
        font-size: 12px;
    }

    .invoice-info h2 {
        color: #455a64;
        font-size: 14px;
    }

    #project p {
        font-size: 12px;
    }

    #invoice-table {
        padding: 30px 5px;
    }

    #invoice-table h2 {
        font-size: 14px;
    }

    .table {
        width: 100%;
        max-width: 100%;
        margin-bottom: 1rem;
        background-color: transparent;
    }

    .table > tbody > tr > td,
    .table > tbody > tr > th,
    .table > tfoot > tr > td,
    .table > tfoot > tr > th,
    .table > thead > tr > td,
    .table > thead > tr > th {
        vertical-align: center;
    }

    .table td, .table th {
        vertical-align: center;
        border-top: 1px solid #dee2e6;
    }

    .table-responsive {
        display: block;
        width: 100%;
    }

    .tabletitle {
        background: #e7e7e7;
    }

    .table-item {
        width: 50%;
    }

    .itemtext {
        font-size: .9em;
    }

    #legalcopy p {
        font-size: 12px;
    }

</style>

</body></html>