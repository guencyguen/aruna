<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 6/1/2018
 * Time: 11:54 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ARUNA</title>

    <?php echo $css; ?>
</head>
<body class="fix-header fix-sidebar">
<?php echo $preloader; ?>

<div id="main-wrapper">
    <?php echo $layout;?>

    <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text">Purchase Order <?php echo $order[0]['po']; ?></h4>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Purchase Order</li>
                    <li class="breadcrumb-item active">Detail Purchase Order</li>
                </ol>
            </div>
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div id="invoice" class="effect2">
                                <div id="invoice-top">
                                    <div style="float: left">
                                        <img src="<?php echo base_url('/assets/icons/aruna2.png') ?>" width="100px" height="100px" />
                                    </div>
                                    <div class="title" style="margin-top: 25px">
                                        <h4>Purchase Order</h4>
                                        <p>Nomor: <?php echo $order[0]['po']; ?><br>
                                            Tanggal PO: <?php echo date_format(new DateTime($order[0]['po_date']), 'd M Y'); ?><br>
                                            Mata Uang: Rupiah
                                        </p>
                                    </div>
                                </div>
                                <hr>
                                <div id="invoice-mid">
                                    <div class="invoice-info">
                                        <p>Supplier:</p>
                                        <h2 style="margin-bottom: 0"><strong><?php echo $supplier['s_nama']; ?></strong></h2>
                                        <h6><?php echo $supplier['s_address']; ?><br>
                                            <?php echo $supplier['s_city']; ?>, <?php echo $supplier['s_province']; ?>, <?php echo $supplier['s_country']; ?> - <?php echo $supplier['s_postal_code']; ?><br>
                                            <?php echo $supplier['s_telp']; ?>
                                        </h6>
                                    </div>
                                    <div id="project">
                                        <p>Pemesan:</p>
                                        <h6><strong><?php echo $order[0]['em_name']; ?></strong></h6>
                                        <h6><?php echo $em_position[0]['pos'] ?><br>
                                            <?php echo $order[0]['em_phone_number']; ?>
                                        </h6>
                                    </div>
                                </div>

                                <div id="invoice-bot">
                                    <div id="invoice-table">
                                        <div class="table-responsive">
                                            <table class="table" width="100%">
                                                <tr class="tabletitle">
                                                    <td class="table-item">
                                                        <h2>Nama Barang</h2>
                                                    </td>
                                                    <td class="Hours">
                                                        <h2>Jumlah</h2>
                                                    </td>
                                                    <td class="Rate">
                                                        <h2>Harga Satuan</h2>
                                                    </td>
                                                    <td class="subtotal">
                                                        <h2>Total Harga</h2>
                                                    </td>
                                                </tr>

                                                <?php
                                                $sum = 0;
                                                foreach ($order as $item) {
                                                    echo '<tr class="service">';
                                                    echo '<td class="tableitem"><p class="itemtext">'.$item['pr_name'].'</p></td>';
                                                    echo '<td class="tableitem"><p class="itemtext">'.$item['qty'].' unit</p></td>';
                                                    echo '<td class="tableitem"><p class="itemtext">'.number_format($item['price'], 0).'</p></td>';
                                                    echo '<td class="tableitem"><p class="itemtext">'.number_format($item['total_price'], 0).'</p></td>';
                                                    echo '</tr>';
                                                    $sum += $item['total_price'];
                                                }
                                                ?>

                                                <tr class="tabletitle">
                                                    <td></td>
                                                    <td></td>
                                                    <td class="Rate">
                                                        <h2>Total</h2>
                                                    </td>
                                                    <td class="payment">
                                                        <h2>Rp. <?php echo number_format($sum, 0); ?></h2>
                                                    </td>
                                                </tr>

                                            </table>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <a href="<?php echo site_url('purchaseorder/downloadPo/'.$order[0]['po']); ?>" class="btn btn-info">Unduh PO</a>
                            <a href="<?php echo site_url('purchaseorder/detailorder/'.$order[0]['o_id']); ?>" class="btn btn-danger">Kembali</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Page Content -->
        </div>
        <!-- End Container fluid  -->

        <?php echo $footer; ?>
    </div>

</div>

</body>
<?php echo $js; ?>
<script>
    $(document).ready(function () {

    })
</script>
</html>