<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 6/1/2018
 * Time: 11:54 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ARUNA</title>

    <?php echo $css; ?>
    <link href="<?php echo base_url('assets/css/lib/sweetalert/sweetalert.css'); ?>" rel="stylesheet">
</head>
<body class="fix-header fix-sidebar">
<?php echo $preloader; ?>

<div id="main-wrapper">
    <?php echo $layout;?>

    <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text">Detail Order <?php echo $order[0]['o_id']; ?></h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Purchase Order</li>
                    <li class="breadcrumb-item active">Detail Order</li>
                </ol>
            </div>
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <ul class="nav nav-tabs customtab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#list_items" role="tab"><span class="hidden-xs-down">Daftar Barang</span></a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#purchase_order" role="tab"><span class="hidden-xs-down">Purchase Order</span></a> </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="list_items" role="tabpanel">
                                    <div class="p-20">
                                        <form method="post" enctype="multipart/form-data">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label class="control-label col-md-2">Dipesan Oleh</label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static"><?php echo $order[0]['em_name'] ?></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="control-label col-md-2">Tanggal Pesan</label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static"><?php echo date_format(new DateTime($order[0]['o_date']), 'd M Y') ?></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="control-label col-md-2">Barang:</label>
                                                    </div>
                                                    <div class="table-responsive">
                                                        <table id="productTable" class="display nowrap table table-hover table-bordered" cellspacing="0" width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th>Nama Barang</th>
                                                                <th>Jumlah</th>
                                                                <th>Harga Satuan</th>
                                                                <th>Total Harga</th>
                                                                <th>Supplier</th>
                                                                <th class="text-left">No. PO</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                            foreach ($items as $row) {
                                                                echo "<tr>";
                                                                echo "<td>".$row['pr_name']."</td>";
                                                                echo "<td class='text-right'>".number_format($row['qty'],0)." pcs</td>";
                                                                echo "<td class='text-right'> Rp. ".number_format($row['price'],0)."</td>";
                                                                echo "<td class='text-right'> Rp. ".number_format($row['total_price'],0)."</td>";
                                                                echo "<td>".$row['s_name']."</td>";
                                                                if (is_null($row['po']))
                                                                    echo "<td class='text-center'><span class='badge badge-success'>Belum ada PO</span></td>";
                                                                else echo "<td class='text-center'>".$row['po']."</td>";
                                                                echo "</tr>";
                                                            }
                                                            ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 m-t-35">
                                                    <div class="form-group">
                                                        <a href="<?php echo site_url('purchaseorder/index'); ?>" class="btn btn-danger">Kembali</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="tab-pane" id="purchase_order" role="tabpanel">
                                    <div class="p-20">
                                        <form method="post" enctype="multipart/form-data">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label class="control-label col-md-2">Dipesan Oleh</label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static"><?php echo $order[0]['em_name'] ?></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="control-label col-md-2">Tanggal Pesan</label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static"><?php echo date_format(new DateTime($order[0]['o_date']), 'd M Y') ?></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="control-label col-md-2">Purchase Order:</label>
                                                    </div>
                                                    <div class="table-responsive">
                                                        <table id="POTable" class="display nowrap table table-hover table-bordered" cellspacing="0" width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th>No. PO</th>
                                                                <th>Supplier</th>
                                                                <th>Jumlah Barang</th>
                                                                <th>Total Harga</th>
                                                                <th>Aksi</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                            foreach ($po as $row) {
                                                                echo "<tr>";
                                                                if (is_null($row['po']))
                                                                    echo "<td><span class='badge badge-success'>Belum ada PO</span></td>";
                                                                else echo "<td>".$row['po']."</td>";
                                                                echo "<td>".$row['s_name']."</td>";
                                                                echo "<td class='text-right'>".number_format($row['total_item'],0)." pcs</td>";
                                                                echo "<td class='text-right'> Rp. ".number_format($row['total_price'],0)."</td>";
                                                                echo "<td class='text-center'>";
                                                                if (is_null($row['po']))
                                                                    echo "<a href='".site_url('purchaseorder/generatePo/').$row['o_id']."/".$row['s_id']."'><i class='mdi mdi-file-document'></i> Buat PO</a>";
                                                                else {
                                                                    echo "<a href='".site_url('purchaseorder/detailPo/').$row['po']."'><i class='mdi mdi-eye'></i></a> ";
                                                                    echo "<a href='#' data-id='".$row['po']."' class='sweet-confirm'><i class='mdi mdi-delete'></i></a>";
                                                                }
                                                                echo "</td>";
                                                                echo "</tr>";
                                                            }
                                                            ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Page Content -->
        </div>
        <!-- End Container fluid  -->

        <?php echo $footer; ?>
    </div>

    <div style="display: none">
        <form id="delete-form" method="POST"></form>
    </div>

</div>

</body>
<?php echo $js; ?>
<!-- Datatable & buttons -->
<script src="<?php echo base_url('/assets/js/lib/datatables/datatables.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js'); ?>"></script>
<!-- Delete alert -->
<script src="<?php echo base_url('assets/js/lib/sweetalert/sweetalert.min.js'); ?>"></script>
<!-- Modified buttons -->
<script>
    $(document).ready(function () {
        $('#productTable').DataTable({
            dom: 'Bfrtip',
            buttons: [{ //customized datatable button
                extend: "excel",
                text: "<i class='fa fa-table'></i>",
                exportOptions: {
                    columns: ":not(:last-child)"
                }
            }, {
                extend: "pdf",
                text: "<i class='fa fa-file-pdf-o'></i>",
                exportOptions: {
                    columns: ":not(:last-child)"
                }
            }, {
                extend: "print",
                text: "<i class='fa fa-print'></i>",
                exportOptions: {
                    columns: ":not(:last-child)"
                }
            }]
        });
        $('#POTable').DataTable({
            dom: 'Bfrtip',
            buttons: [{ //customized datatable button
                extend: "excel",
                text: "<i class='fa fa-table'></i>",
                exportOptions: {
                    columns: ":not(:last-child)"
                }
            }, {
                extend: "pdf",
                text: "<i class='fa fa-file-pdf-o'></i>",
                exportOptions: {
                    columns: ":not(:last-child)"
                }
            }, {
                extend: "print",
                text: "<i class='fa fa-print'></i>",
                exportOptions: {
                    columns: ":not(:last-child)"
                }
            }]
        });
        $('#POTable tbody').on('click', '.sweet-confirm', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            swal({
                    title: "Konfirmasi",
                    text: "Apakah Anda yakin akan membatalkan PO " + id + "?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ya",
                    cancelButtonText: "Tidak",
                    closeOnConfirm: true
                },
                function(){
                    $('#delete-form').attr('action', '<?php echo site_url('purchaseorder/cancelPo/'); ?>' + id).submit();
                });
        });
    })
</script>
</html>