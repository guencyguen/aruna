<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 6/1/2018
 * Time: 3:36 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ARUNA</title>
    <?php echo $css; ?>
</head>
<body class="fix-header fix-sidebar">
<?php echo $preloader; ?>

<div id="main-wrapper">
    <?php echo $layout;?>

    <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text">Edit Barang #<?php echo $product['pr_id']; ?></h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Manajemen Barang</li>
                    <li class="breadcrumb-item">Daftar Barang</li>
                    <li class="breadcrumb-item active">Edit Barang</li>
                </ol>
            </div>
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="post" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Nama Barang</label>
                                            <input name="nama" id="nama" type="text" class="form-control" value="<?php echo $product['pr_name']; ?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Harga Jual (Rp)</label>
                                            <input name="harga_jual" id="harga_jual" type="number" min="1" class="form-control" value="<?php echo $product['harga_jual']; ?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Stok Toko</label>
                                            <input name="stok_toko" id="stok_toko" type="number" min="0" class="form-control" value="<?php echo $product['pr_stock']; ?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Supplier</label>
                                            <select name="supplier" id="supplier" class="form-control select-search" required>
                                                <?php
                                                foreach ($suppliers as $row) {
                                                    echo "<option value='".$row['s_id']."'>".$row['s_nama']."</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Kategori</label>
                                            <select name="kategori" id="kategori" class="form-control select-search" required>
                                                <option value="Roti/kue">Roti/kue</option>
                                                <option value="Selai">Selai</option>
                                                <option value="chiki">Kerupuk</option>
                                                <option value="Bumbu masakan">Bumbu masakan</option>
                                                <option value="Minuman ringan & soda">Minuman ringan & soda</option>
                                                <option value="Susu">Susu</option>
                                                <option value="Perlengkapan bayi">Perlengkapan bayi</option>
                                                <option value="Perlengkapan mandi">Perlengkapan mandi</option>
                                                <option value="Kosmetik">Kosmetik</option>
                                                <option value="Pembersih pakaian">Pembersih pakaian</option>
                                                <option value="Peralatan dapur">Peralatan dapur</option>
                                                <option value="Peralatan rumah tangga">Peralatan rumah tangga</option>
                                                <option value="Lain-lain">Lain-lain</option>
                                            </select>
                                        </div>
                                        <div class="form-group p-t-4">
                                            <label>Harga Beli (Rp)</label>
                                            <input name="harga_beli" id="harga_beli" type="number" min="1" class="form-control" value="<?php echo $product['harga_beli']; ?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Stok Gudang</label>
                                            <input name="stok_gudang" id="stok_gudang" type="number" min="0" class="form-control" value="<?php echo $product['pr_inventory']; ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-info">Update</button>
                                            <a href="<?php echo site_url('product/index'); ?>" class="btn btn-danger">Batal</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Page Content -->
        </div>
        <!-- End Container fluid  -->

        <?php echo $footer; ?>
    </div>

</div>

</body>
<?php echo $js; ?>
<script>
    $(function () {
        $('#kategori').val('<?php echo $product['pr_category']; ?>').trigger('change');
        $('#supplier').val('<?php echo $product['s_id']; ?>').trigger('change');
        $('.select-search').select2();
    })
</script>
<script src="<?php echo base_url('/assets/js/lib/bootstrap/js/bootstrap_select.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/bootstrap/js/select2.min.js'); ?>"></script>
</html>