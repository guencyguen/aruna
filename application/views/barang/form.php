<?php
/**
 * Created by HayooSiapa hayooo.
 * User: HAUW
 * Date: 19/06/2018
 * Time: 19:24 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ARUNA</title>

	<link rel="shortcut icon" href="<?php echo base_url("assets/icons/arulogo.png"); ?>">
	<?php echo $css; ?>
</head>

<body>
	<?php echo $preloader; ?>
	<div id="main-wrapper">
    <?php echo $layout;?>

	    <div class="page-wrapper">
	        <div class="row page-titles">
	            <div class="col-md-5 align-self-center">
	                <h3 class="text">Unggah Daftar Barang</h3>
	            </div>
	            <div class="col-md-7 align-self-center">
	                <ol class="breadcrumb">
	                    <li class="breadcrumb-item">Manajemen Barang</li>
	                    <li class="active breadcrumb-item">Unggah Daftar Barang</li>
	                </ol>
	            </div>
	        </div>

	        <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <form id="uploadform" method="post" enctype="multipart/form-data" class="form-horizontal">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input id="files" class="files" type="file" name="files" hidden>
                                            <div class="form-group row">
                                                <input type="text" class="form-control col-md-3" disabled placeholder="Belum ada file">
                                                <label for="files" class="form-group btn btn-primary browse m-l-5"><i class="mdi mdi-file-excel"></i> Pilih file</label>
                                                <a class="form-group m-l-5 btn btn-warning" href="<?php echo base_url("excel/formatproduct.xlsx"); ?>"><i class='mdi mdi-download'></i> Unduh format</a>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <button disabled name="uploadfile" type="submit" id="submit" class="btn btn-info">Tinjau</button>
                                            </div>
                                            <?php echo $status ?>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <?php if ($preview) { ?>
                        <div class="card">
                            <div class="card-body">
                                <form method="post" enctype="multipart/form-data" class="form-horizontal">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="table-responsive m-t-20">
                                                <h4><label class="form-group">Tinjau Unggahan</label></h4>
                                                <table id="productTable" class="display nowrap table table-hover table-bordered" cellspacing="0" width="100%">
                                                    <thead>
                                                    <tr>
                                                        <th>Nama Barang</th>
                                                        <th>Stok Gudang</th>
                                                        <th>Kategori</th>
                                                        <th>Harga Jual</th>
                                                        <th>Harga Beli</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    foreach ($sheet as $key => $row) {
                                                        if ($key == 1) continue; // skip header
                                                        if ($row['A'] == "") continue; // skip null rows
                                                        echo "<tr>";
                                                        echo "<td>".$row['A']."</td>";
                                                        echo "<td>".$row['B']."</td>";
                                                        echo "<td>".$row['C']."</td>";
                                                        echo "<td class='text-right'> Rp. ".number_format($row['D'],0)."</td>";
                                                        echo "<td class='text-right'> Rp. ".number_format($row['E'],0)."</td>";
                                                        echo "</tr>";
                                                    }
                                                    ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input name="filename" value="<?php echo $filename; ?>" hidden>
                                                <button type="submit" name="preview" class="btn btn-info">Tambah</button>
                                                <a href="<?php echo site_url('product/form'); ?>" class="btn btn-danger">Batal</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <?php } ?>

                    </div>
                </div>

			</div>
			<?php echo $footer; ?>
		</div>
	</div>	
</body>
<?php echo $js; ?>
<script src="<?php echo base_url('/assets/js/lib/datatables/datatables.min.js'); ?>"></script>
<script>
    $(document).ready(function(){
        $("#productTable").DataTable();
        $("#files").on('change', function () {
            $(this).parent().find('.form-group .form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
            $("#submit").removeAttr("disabled");
        });
    });
</script>
</html>
