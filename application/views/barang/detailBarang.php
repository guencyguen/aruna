<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 6/1/2018
 * Time: 11:54 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ARUNA</title>
    <?php echo $css; ?>
</head>
<body class="fix-header fix-sidebar">
<?php echo $preloader; ?>

<div id="main-wrapper">
    <?php echo $layout;?>

    <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text">Detail Barang #<?php echo $product['pr_id']; ?></h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Manajemen Barang</li>
                    <li class="breadcrumb-item">Daftar Barang</li>
                    <li class="breadcrumb-item active">Detail Barang</li>
                </ol>
            </div>
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="post" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label">Nama Barang</label>
                                            <div class="col-sm-10">
                                                <p class="form-control-static"><?php echo $product['pr_name']; ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Harga Jual</label>
                                            <div class="col-sm-10">
                                                <p class="form-control-static">Rp <?php echo number_format($product['harga_jual'],0); ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Stok Toko</label>
                                            <div class="col-sm-10">
                                                <p class="form-control-static"><?php echo $product['pr_stock']; ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Supplier</label>
                                            <div class="col-sm-10">
                                                <p class="form-control-static"><?php echo $supplier['s_nama']; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label">Kategori</label>
                                            <div class="col-sm-10">
                                                <p class="form-control-static"><?php echo $product['pr_category']; ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Harga Beli</label>
                                            <div class="col-sm-10">
                                                <p class="form-control-static">Rp <?php echo number_format($product['harga_beli'],0); ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Stok Gudang</label>
                                            <div class="col-sm-10">
                                                <p class="form-control-static"><?php echo $product['pr_inventory']; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <a href="<?php echo site_url('product/edit/').$product['pr_id']; ?>" class="btn btn-info">Edit</a>
                                            <a href="<?php echo site_url('product/index'); ?>" class="btn btn-danger">Kembali</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Page Content -->
        </div>
        <!-- End Container fluid  -->

        <?php echo $footer; ?>
    </div>

</div>

</body>
<?php echo $js; ?>
</html>