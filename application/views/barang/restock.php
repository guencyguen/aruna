<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 6/2/2018
 * Time: 12:24 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ARUNA</title>
    <?php echo $css; ?>
</head>
<body class="fix-header fix-sidebar">
<?php echo $preloader; ?>

<div id="main-wrapper">
    <?php echo $layout;?>

    <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text">Restock Barang Toko</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Manajemen Barang</li>
                    <li class="breadcrumb-item active">Restock Barang Toko</li>
                </ol>
            </div>
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="post" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group row">
                                            <label class="control-label col-md-2">Restock Barang:</label>
                                        </div>
                                        <div class="row m-l-15">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label">Nama Barang</label>
                                                    <select name="barang[]" id="barang" class="form-control select-search" required>
                                                        <option value="" selected disabled>Pilih barang...</option>
                                                        <?php
                                                        foreach ($products as $row) {
                                                            echo "<option value='".$row['pr_id']."' data-max='".$row['pr_inventory']."'>".$row['pr_name']."</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="control-label">Jumlah</label>
                                                    <input type="number" name="qty[]" id="qty" class="form-control" min="0" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="moreItem"></div>
                                    </div>
                                    <div class="col-md-12 m-l-25">
                                        <div class="form-group">
                                            <button type="button" id="addItem" class="btn btn-outline-primary btn-sm"><i class="mdi mdi-plus"></i></button>
                                            <button type="button" id="removeItem" class="btn btn-sm btn-outline-danger"><i class="mdi mdi-minus"></i></button>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-info">Restock</button>
                                            <a href="<?php echo site_url('product/index'); ?>" class="btn btn-danger">Batal</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Page Content -->
        </div>
        <!-- End Container fluid  -->

        <?php echo $footer; ?>
    </div>

</div>

</body>
<?php echo $js; ?>
<script>
    $(function () {
        $('#addItem').click(function () {
            var script = '<?php foreach ($products as $row) { echo "<option value=\'".$row['pr_id']."\' data-max=\'".$row['pr_inventory']."\'>".$row['pr_name']."</option>";}?>';
            $('.moreItem').append(
                '<div class="row m-l-15 new">'+
                '    <div class="col-md-4">'+
                '        <div class="form-group">'+
                '            <select name="barang[]" id="barang" class="form-control select-search" required>'+
                '                <option value="" selected disabled>Pilih barang...</option>' +
                script +
                '            </select>'+
                '        </div>'+
                '    </div>'+
                '    <div class="col-md-2">'+
                '        <div class="form-group">'+
                '            <input type="number" name="qty[]" id="qty" class="form-control" min="0" required>'+
                '        </div>'+
                '    </div>'+
                '</div>'
            );
            $('.select-search').select2();
        });
        $('#removeItem').click(function () {
            $('.new:last').remove();
        });
        $('.select-search').select2();
        $('select').change(function () {
            $('input[type=number]').attr('max', $(this).find(':selected').data('max'));
        })
    })
</script>
<script src="<?php echo base_url('/assets/js/lib/bootstrap/js/bootstrap_select.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/bootstrap/js/select2.min.js'); ?>"></script>
</html>