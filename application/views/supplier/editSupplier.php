<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 6/1/2018
 * Time: 3:36 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ARUNA</title>
    <?php echo $css; ?>
</head>
<body class="fix-header fix-sidebar">
<?php echo $preloader; ?>

<div id="main-wrapper">
    <?php echo $layout;?>

    <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text">Edit Supplier #<?php echo $supplier['s_id']; ?></h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Manajemen Supplier</li>
                    <li class="breadcrumb-item">Daftar Supplier</li>
                    <li class="breadcrumb-item active">Edit Supplier</li>
                </ol>
            </div>
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form role="form" method="post" enctype="multipart/form-data" action="<?php echo site_url('supplier/edit/').$supplier['s_id']; ?>">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Nama Supplier</label>
                                            <input name="nama" id="nama" type="text" maxlength="50" class="form-control" value="<?php echo $supplier['s_nama']; ?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat</label>
                                            <input name="alamat" id="alamat" type="text" maxlength="120" class="form-control" placeholder="Nama jalan, RT/RW, Kecamatan, Kelurahan" value="<?php echo $supplier['s_address']; ?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Kota</label>
                                            <input name="kota" id="kota" type="text" maxlength="30" class="form-control" value="<?php echo $supplier['s_city']; ?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Kode Pos</label>
                                            <input name="kodepos" id="kodepos" type="text" maxlength="10" class="form-control" value="<?php echo $supplier['s_postal_code']; ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>E-mail</label>
                                            <input name="email" id="email" type="email" class="form-control" value="<?php echo $supplier['s_email']; ?>">
                                        </div>
                                        <div class="form-group p-t-4">
                                            <label>Nomor Telpon</label>
                                            <input name="notelp" id="notelp" type="text" maxlength="12" class="form-control" value="<?php echo $supplier['s_telp']; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Provinsi</label>
                                            <input name="provinsi" id="provinsi" type="text" maxlength="30" class="form-control" value="<?php echo $supplier['s_province']; ?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Negara</label>
                                            <input name="negara" id="negara" type="text" maxlength="30" class="form-control" value="<?php echo $supplier['s_country']; ?>" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-info">Update</button>
                                            <a href="<?php echo site_url('supplier/index'); ?>" class="btn btn-danger">Batal</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Page Content -->
        </div>
        <!-- End Container fluid  -->

        <?php echo $footer; ?>
    </div>

</div>

</body>
<?php echo $js; ?>
</html>