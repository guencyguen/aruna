<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 5/31/2018
 * Time: 9:30 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ARUNA</title>
    <link href="<?php echo base_url('assets/css/lib/sweetalert/sweetalert.css'); ?>" rel="stylesheet">
    <?php echo $css; ?>
</head>
<body class="fix-header fix-sidebar">
<?php echo $preloader; ?>

<div id="main-wrapper">
    <?php echo $layout;?>

    <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text">Daftar Supplier</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Manajemen Supplier</li>
                    <li class="breadcrumb-item active">Daftar Supplier</li>
                </ol>
            </div>
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <a href="<?php echo site_url('supplier/addsupplier') ?>" class="btn btn-primary"><i class="mdi mdi-plus-circle"></i> Tambah supplier</a>
                            <div class="table-responsive m-t-20">
                                <table id="supplierTable" class="display nowrap table table-hover table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>ID Supplier</th>
                                            <th>Nama Supplier</th>
                                            <th>E-mail</th>
                                            <th>No. Telpon</th>
                                            <th>Alamat</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        foreach ($suppliers as $row) {
                                            echo "<tr>";
                                            echo "<td>".$row['s_id']."</td>";
                                            echo "<td>".$row['s_nama']."</td>";
                                            echo "<td>".$row['s_email']."</td>";
                                            echo "<td>".$row['s_telp']."</td>";
                                            echo "<td>".$row['s_address']."</td>";
                                            echo "<td class='text-center'>";
                                            echo "      <a href='".site_url('supplier/edit/').$row['s_id']."'><i class='mdi mdi-pencil'></i></a> ";
                                            echo "      <a href='".site_url('supplier/detail/').$row['s_id']."'><i class='mdi mdi-eye'></i></a> ";
                                            echo "      <a href='#' data-id='".$row['s_id']."' data-name='".$row['s_nama']."' class='sweet-confirm'><i class='mdi mdi-delete'></i></a>";
                                            echo "</td>";
                                            echo "</tr>";
                                        }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Page Content -->
        </div>
        <!-- End Container fluid  -->

        <?php echo $footer; ?>
    </div>

    <div style="display: none">
        <form id="delete-form" method="POST"></form>
    </div>

</div>

</body>
<?php echo $js; ?>
<!-- Datatable & buttons -->
<script src="<?php echo base_url('/assets/js/lib/datatables/datatables.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js'); ?>"></script>
<!-- Delete alert -->
<script src="<?php echo base_url('assets/js/lib/sweetalert/sweetalert.min.js'); ?>"></script>
<!-- Modified buttons -->
<script>
    $(document).ready(function () {
        $('#supplierTable').DataTable({
            dom: 'Bfrtip',
            buttons: [{ //customized datatable button
                extend: "excel",
                text: "<i class='fa fa-table'></i>",
                exportOptions: {
                    columns: ":not(:last-child)"
                }
            }, {
                extend: "pdf",
                text: "<i class='fa fa-file-pdf-o'></i>",
                exportOptions: {
                    columns: ":not(:last-child)"
                }
            }, {
                extend: "print",
                text: "<i class='fa fa-print'></i>",
                exportOptions: {
                    columns: ":not(:last-child)"
                }
            }]
        });
        $('#supplierTable tbody').on('click', '.sweet-confirm', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var name = $(this).data('name');
            swal({
                    title: "Konfirmasi",
                    text: "Apakah Anda yakin akan menghapus supplier " + name + "?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ya",
                    cancelButtonText: "Tidak",
                    closeOnConfirm: true
                },
                function(){
                    $('#delete-form').attr('action', '<?php echo site_url('supplier/delete/'); ?>' + id).submit();
                });
        });
    })
</script>
</html>