<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 6/1/2018
 * Time: 11:54 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ARUNA</title>
    <?php echo $css; ?>
</head>
<body class="fix-header fix-sidebar">
<?php echo $preloader; ?>

<div id="main-wrapper">
    <?php echo $layout;?>

    <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text">Detail Supplier #<?php echo $supplier['s_id']; ?></h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Manajemen Supplier</li>
                    <li class="breadcrumb-item">Daftar Supplier</li>
                    <li class="breadcrumb-item active">Detail Supplier</li>
                </ol>
            </div>
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <ul class="nav nav-tabs customtab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#detail" role="tab"><span class="hidden-xs-down">Detail Supplier</span></a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#item_supplier" role="tab"><span class="hidden-xs-down">Daftar Barang</span></a> </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="detail" role="tabpanel">
                                    <div class="p-20">
                                        <form method="post" enctype="multipart/form-data">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Nama Supplier</label>
                                                        <div class="col-sm-10">
                                                            <p class="form-control-static"><?php echo $supplier['s_nama']; ?></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Alamat</label>
                                                        <div class="col-sm-10">
                                                            <p class="form-control-static"><?php echo $supplier['s_address']; ?></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Kota</label>
                                                        <div class="col-sm-10">
                                                            <p class="form-control-static"><?php echo $supplier['s_city']; ?></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Kode Pos</label>
                                                        <div class="col-sm-10">
                                                            <p class="form-control-static"><?php echo $supplier['s_postal_code']; ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="control-label">E-mail</label>
                                                        <div class="col-sm-10">
                                                            <p class="form-control-static"><?php echo $supplier['s_email']; ?></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Nomor Telpon</label>
                                                        <div class="col-sm-10">
                                                            <p class="form-control-static"><?php echo $supplier['s_telp']; ?></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Provinsi</label>
                                                        <div class="col-sm-10">
                                                            <p class="form-control-static"><?php echo $supplier['s_province']; ?></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Negara</label>
                                                        <div class="col-sm-10">
                                                            <p class="form-control-static"><?php echo $supplier['s_country']; ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <a href="<?php echo site_url('supplier/edit/').$supplier['s_id']; ?>" class="btn btn-info">Edit</a>
                                                        <a href="<?php echo site_url('supplier/index'); ?>" class="btn btn-danger">Kembali</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="tab-pane" id="item_supplier" role="tabpanel">
                                    <div class="p-20">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="control-label">Nama Supplier</label>
                                                    <div class="col-sm-10">
                                                        <p class="form-control-static"><?php echo $supplier['s_nama']; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table id="productTable" class="display nowrap table table-hover table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th>ID Barang</th>
                                                <th>Nama Barang</th>
                                                <th>Kategori</th>
                                                <th>Tanggal Terima Terakhir</th>
                                                <th>Harga Beli</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($products as $row) {
                                                echo "<tr>";
                                                echo "<td>".$row['pr_id']."</td>";
                                                echo "<td>".$row['pr_name']."</td>";
                                                echo "<td>".$row['pr_category']."</td>";
                                                echo "<td class='text-center'>".date_format(new DateTime($row['date_received']), 'd M Y')."</td>";
                                                echo "<td class='text-right'>".number_format($row['harga_beli'],0)."</td>";
                                                echo "</tr>";
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Page Content -->
        </div>
        <!-- End Container fluid  -->

        <?php echo $footer; ?>
    </div>

</div>

</body>
<?php echo $js; ?>
<!-- Datatable & buttons -->
<script src="<?php echo base_url('/assets/js/lib/datatables/datatables.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js'); ?>"></script>
<!-- Delete alert -->
<script src="<?php echo base_url('assets/js/lib/sweetalert/sweetalert.min.js'); ?>"></script>
<!-- Modified buttons -->
<script>
    $(document).ready(function () {
        $('#productTable').DataTable({
            dom: 'Bfrtip',
            buttons: [{ //customized datatable button
                extend: "excel",
                text: "<i class='fa fa-table'></i>",
                exportOptions: {
                    columns: ":not(:last-child)"
                }
            }, {
                extend: "pdf",
                text: "<i class='fa fa-file-pdf-o'></i>",
                exportOptions: {
                    columns: ":not(:last-child)"
                }
            }, {
                extend: "print",
                text: "<i class='fa fa-print'></i>",
                exportOptions: {
                    columns: ":not(:last-child)"
                }
            }]
        });
    })
</script>
</html>