<script src="<?php echo base_url('/assets/js/lib/jquery/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/bootstrap/js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/bootstrap/js/popper.min.js'); ?>"></script>

<script src="<?php echo base_url('/assets/js/jquery.slimscroll.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/sidebarmenu.js'); ?>"></script>

<script src="<?php echo base_url('/assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js') ?>"></script>

<script src="<?php echo base_url('/assets/js/scripts.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/custom.min.js'); ?>"></script>