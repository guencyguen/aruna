<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 5/26/2018
 * Time: 12:13 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ARUNA - Masuk</title>
    <?php echo $css; ?>
</head>
<body class="fix-header fix-sidebar">
    <?php echo $preloader; ?>

    <div id="main-wrapper">

        <div class="unix-login">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-4">
                        <div class="login-content card">
                            <div class="login-form">
                                <h4>Masuk ke ARUNA</h4>
                                <form action="<?php echo base_url('index.php/login/proceed_login'); ?>" method="post">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input name="username" type="text" class="form-control" placeholder="Username" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Kata sandi</label>
                                        <input name="password" type="password" class="form-control" placeholder="Kata sandi" required>
                                    </div>
                                    <?php if($this->session->has_userdata('invalid_login_message')): ?>
                                        <p class="text-danger">
                                            <?php echo $this->session->invalid_login_message; ?>
                                        </p>
                                    <?php endif; ?>
                                    <button type="submit" class="btn btn-primary btn-flat m-b-30 m-t-30">Masuk</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</body>
<?php echo $js; ?>
</html>