<?php

defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ARUNA</title>
    <?php echo $css; ?>
</head>
<body class="fix-header fix-sidebar">
<?php echo $preloader; ?>

<div id="main-wrapper">
    <?php echo $layout;?>

    <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text">Detail Pelanggan #<?php echo $member['mem_id']; ?></h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Manajemen Pelanggan</li>
                    <li class="breadcrumb-item">Daftar Pelanggan</li>
                    <li class="breadcrumb-item active">Detail Pelanggan</li>
                </ol>
            </div>
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="post" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label">Nama Pelanggan</label>
                                            <div class="col-sm-10">
                                                <p class="form-control-static"><?php echo $member['mem_fullname']; ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Tanggal Lahir</label>
                                            <div class="col-sm-10">
                                                <p class="form-control-static"><?php echo $tanggal ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Jenis Kelamin</label>
                                            <div class="col-sm-10">
                                                <p class="form-control-static">
                                                    <?php 
                                                        if($member['mem_gender']=="female") echo "Perempuan";
                                                        else if($member['mem_gender']=="male") echo "Laki-Laki"; 
                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label">Alamat</label>
                                            <div class="col-sm-10">
                                                <p class="form-control-static"><?php echo $member['mem_address']; ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">E-mail</label>
                                            <div class="col-sm-10">
                                                <p class="form-control-static"><?php echo $member['mem_email']; ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Poin</label>
                                            <div class="col-sm-10">
                                                <p class="form-control-static"><?php echo $member['poin']; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <a href="<?php echo site_url('pelanggan/edit/').$member['mem_id']; ?>" class="btn btn-info">Edit</a>
                                            <a href="<?php echo site_url('pelanggan/index'); ?>" class="btn btn-danger">Kembali</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Page Content -->
        </div>
        <!-- End Container fluid  -->

        <?php echo $footer; ?>
    </div>

</div>

</body>
<?php echo $js; ?>
</html>