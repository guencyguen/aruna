<?php

defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ARUNA</title>
    <?php echo $css; ?>
</head>
<body class="fix-header fix-sidebar">
<?php echo $preloader; ?>

<div id="main-wrapper">
    <?php echo $layout;?>

    <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text">Cek Poin</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Manajemen Pelanggan</li>
                    <li class="breadcrumb-item active">Cek Poin</li>
                </ol>
            </div>
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form role="form" method="post" enctype="multipart/form-data" action="<?php echo site_url('pelanggan/poin/'); ?>">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Masukan ID Pelanggan</label>
                                            <input name="mem_id" id="mem_id" type="text" maxlength="50" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-info">Cek Poin</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Page Content -->
        </div>
        <!-- End Container fluid  -->

        <?php if($cek == true){ ?>
               <div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <?php

                                            echo '<div class="form-group">';
                                                echo '<label class="control-label">ID Pelanggan</label>';
                                                echo '<div class="col-sm-10">';
                                                    echo '<p class="form-control-static">' . $member['mem_id'] . '</p>';
                                                echo '</div>';
                                            echo '</div>';

                                            echo '<div class="form-group">';
                                                echo '<label class="control-label">Nama Pelanggan</label>';
                                                echo '<div class="col-sm-10">';
                                                    echo '<p class="form-control-static">' . $member['mem_fullname'] . '</p>';
                                                echo '</div>';
                                            echo '</div>';

                                            echo '<div class="form-group">';
                                                echo '<label class="control-label">Jumlah Poin</label>';
                                                echo '<div class="col-sm-10">';
                                                    echo '<p class="form-control-static">' . $member['poin'] . '</p>';
                                                echo '</div>';
                                            echo '</div>';
                                         ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Page Content -->
            </div>                      
        <?php } ?>

        <?php echo $footer; ?>
    </div>

</div>

</body>
<?php echo $js; ?>
</html>