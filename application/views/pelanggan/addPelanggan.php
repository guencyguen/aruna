<?php

defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ARUNA</title>
    <?php echo $css; ?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('/assets/js/lib/pickers/jquery.datetimepicker.css'); ?>">
</head>
<body class="fix-header fix-sidebar">
<?php echo $preloader; ?>

<div id="main-wrapper">
    <?php echo $layout;?>

    <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text">Tambah Pelanggan Baru</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Manajemen Pelanggan</li>
                    <li class="breadcrumb-item">Daftar Pelanggan</li>
                    <li class="breadcrumb-item active">Tambah Pelanggan</li>
                </ol>
            </div>
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="post" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Nama Pelanggan</label>
                                            <input name="nama" id="nama" type="text" maxlength="50" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Tanggal Lahir</label>
                                            <div class='input-group'>
                                                <input type='text' class="form-control" name="tanggalLahir" id="tanggalLahir" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Jenis Kelamin</label>
                                            <div>
                                                <label class="m-radio m-radio--success">
                                                    <input type="radio" name="gender" value="female">
                                                    Perempuan
                                                    <span></span>
                                                </label>
                                            </div>
                                            <div>
                                                <label class="m-radio m-radio--success">
                                                    <input type="radio" name="gender" value="male">
                                                    Laki-Laki
                                                    <span></span>
                                                </label>
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Alamat</label>
                                            <input name="alamat" id="alamat" type="text" class="form-control" maxlength="120">
                                        </div>
                                        <div class="form-group">
                                            <label>E-mail</label>
                                            <input name="email" id="email" type="email" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Poin</label>
                                            <input name="poin" id="poin" type="number" class="form-control" min="0" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-info">Tambah</button>
                                            <a href="<?php echo site_url('pelanggan/index'); ?>" class="btn btn-danger">Batal</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Page Content -->
        </div>
        <!-- End Container fluid  -->

        <?php echo $footer; ?>
    </div>

</div>

</body>
<?php echo $js; ?>
<script src="<?php echo base_url('/assets/js/lib/pickers/jquery.datetimepicker.full.js'); ?>"></script>
<script>
    $('#tanggalLahir').datetimepicker({
        timepicker : false,
        format:'Y-m-d'
    });
</script>
</html>