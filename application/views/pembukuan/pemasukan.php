<?php
/**
 * Created by Hauw.
 * User: anitut
 * Date: 6/26/2018
 * Time: 18:05 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ARUNA</title>
    <link href="<?php echo base_url('assets/css/lib/sweetalert/sweetalert.css'); ?>" rel="stylesheet">
    <?php echo $css; ?>
</head>
<body class="fix-header fix-sidebar">
<?php echo $preloader; ?>

<div id="main-wrapper">
    <?php echo $layout;?>

    <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text">Pemasukan</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Beranda</li>
                    <li class="breadcrumb-item">Pembukuan</li>
                    <li class="breadcrumb-item active">Pemasukan</li>
                </ol>
            </div>
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">                            
                            <div class="table-responsive m-t-20">
                                <table id="pembukuanTable" class="display nowrap table table-hover table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>ID Transaksi</th>
                                            <th>Tanggal Transaksi</th>
                                            <th>ID Pelanggan</th>
                                            <th>Nama Kasir</th>
                                            <th>Jumlah Barang</th>
                                            <th>Total Transaksi</th>
                                            <th>Total Profit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        foreach ($transaction as $row) {
                                            echo "<tr>";
                                            echo "<td>".$row['t_id']."</td>";
                                            $date = date_format(date_create($row['date_transaction']), "d M Y");
                                            echo "<td>".$date."</td>";
                                            echo "<td>".$row['mem_id']."</td>";
                                            echo "<td>".$row['em_name']."</td>";
                                            echo "<td>".$row['qty']."</td>";
                                            echo "<td> Rp. ".number_format($row['total_price'], 0)."</td>";
                                            echo "<td> Rp. ".number_format($row['total_profit'], 0)."</td>";
                                            echo "</tr>";
                                        }
                                    ?>
                                    </tbody>
                                    <tfoot>                                            
                                        <tr>
                                            <td colspan="5" class="text-center color-primary"><strong>Total</strong></td>
                                            <?php echo "<td class='color-primary'><strong> Rp " . number_format($total['total_price'], 0) . "</strong></td>"; ?>
                                            <?php echo "<td class='text-right color-primary'><strong> Rp " . number_format($total['total_profit'], 0) . "</strong></td>"; ?>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Page Content -->
        </div>
        <!-- End Container fluid  -->

        <?php echo $footer; ?>
    </div>

    <div style="display: none">
        <form id="delete-form" method="POST"></form>
    </div>

</div>

</body>
<?php echo $js; ?>
<!-- Datatable & buttons -->
<script src="<?php echo base_url('/assets/js/lib/datatables/datatables.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js'); ?>"></script>
<!-- Delete alert -->
<script src="<?php echo base_url('assets/js/lib/sweetalert/sweetalert.min.js'); ?>"></script>
<!-- Modified buttons -->
<script>
    $(document).ready(function () {
        $('#pembukuanTable').DataTable({
            dom: 'Bfrtip',
            buttons: [{ //customized datatable button
                extend: "excel",
                text: "<i class='fa fa-table'></i>",
                exportOptions: {
                    columns: ":not(:last-child)"
                }
            }, {
                extend: "pdf",
                text: "<i class='fa fa-file-pdf-o'></i>",
                exportOptions: {
                    columns: ":not(:last-child)"
                }
            }, {
                extend: "print",
                text: "<i class='fa fa-print'></i>",
                exportOptions: {
                    columns: ":not(:last-child)"
                }
            }]
        });
    })
</script>
</html>