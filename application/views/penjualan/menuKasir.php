<?php

defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ARUNA</title>

    <link href="<?php echo base_url('assets/css/lib/sweetalert/sweetalert.css'); ?>" rel="stylesheet">
    <?php echo $css; ?>
</head>
<body class="fix-header fix-sidebar">
<?php echo $preloader; ?>

<div id="main-wrapper">
    <?php echo $layout; ?>

    <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text">Menu Kasir</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Manajemen Penjualan</li>
                    <li class="breadcrumb-item active">Menu Kasir</li>
                </ol>
            </div>
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                        <?php if($transaksi == false){ ?>
                            <form role="form" method="post" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Tanggal : <?php echo $tanggal;  ?></label>
                                        </div>
                                        <div class="form-group">
                                            <label>ID Pelanggan</label>
                                            <select name="id_pelanggan" id="id_pelanggan" class="form-control select-search" required>
                                                <option value="" selected disabled>Pilih ID...</option>
                                                <?php
                                                    foreach ($member as $row) {
                                                        echo "<option value='" . $row['mem_id'] . "'>" . $row['mem_id'] . "</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6"></div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>ID Barang</label>
                                            <select name="id_barang" id="id_barang" class="form-control select-search" required>
                                                <option value="" selected disabled>Pilih ID...</option>
                                                <?php
                                                    foreach ($product as $row) {
                                                        echo "<option value='" . $row['pr_id'] . "'>" . $row['pr_id'] . "</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Jumlah Barang</label>
                                            <input name="jumlah_barang" id="jumlah_barang" type="number" class="form-control" min="1" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <?php
                                            if($st == 0) {
                                                echo '<h6 class="text-danger">' . $msg . '</h6>';
                                            }
                                        ?>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">Tambah Barang</button>
                                            <a href="<?php echo site_url('penjualan/index/1'); ?>" class="btn btn-info">Tambah Transaksi</a>
                                            <a href="<?php echo site_url('penjualan/index/1'); ?>" class="btn btn-danger">Batal</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        <?php } ?>

                        <?php if($transaksi == true ){ ?>
                            <form role="form" method="post" enctype="multipart/form-data" action="<?php echo site_url('penjualan/addBarang'); ?>">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Tanggal : <?php echo $tanggal; ?></label>
                                            <input type="hidden" name="t_id" id="t_id" value="<?php echo $transaction['t_id']; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>ID Pelanggan</label>
                                            <select name="id_pelanggan" id="id_pelanggan" class="form-control select-search" required>
                                                <option value="" disabled>Pilih ID...</option>
                                                <?php
                                                    foreach ($member as $row) {
                                                        if($row['mem_id'] == $transaction['mem_id']){
                                                            echo "<option value='".$row['mem_id']."' selected>".$row['mem_id']."</option>";
                                                        }
                                                        echo "<option value='".$row['mem_id']."'>".$row['mem_id']."</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6"></div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>ID Barang</label>
                                            <select name="id_barang" id="id_barang" class="form-control select-search" required>
                                                <option value="" disabled>Pilih ID...</option>
                                                <?php
                                                    foreach ($product as $row) {
                                                        echo "<option value='".$row['pr_id']."'>".$row['pr_id']."</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Jumlah Barang</label>
                                            <input name="jumlah_barang" id="jumlah_barang" type="number" class="form-control" min="1" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <?php
                                            if($st == 0) {
                                                echo '<h6 class="text-danger">' . $msg . '</h6>';
                                            }
                                        ?>
                                        <div class="form-group" id="btnID">
                                            <button type="submit" class="btn btn-primary">Tambah Barang</button>
                                            <a href="<?php echo site_url('penjualan/addTransaksi/').$transaction['t_id']; ?>" class="btn btn-info">Tambah Transaksi</a>
                                            <a href="#" <?php echo "data-t_id='".$transaction['t_id']."'" ?> class="sweet-confirm btn btn-danger">Batal</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        <?php } ?>

                        </div>
                    </div>
                </div>
            </div>
            <!-- End Page Content -->
        </div>
        <!-- End Container fluid  -->


        <?php if($transaksi == true ){ ?>
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-title">
                            <h4>Daftar Barang </h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table" id="tableBarang">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nama Barang</th>
                                            <th>ID Barang</th>
                                            <th>Jumlah</th>
                                            <th>Harga</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $index = 1;
                                            foreach ($product_sale as $row) {
                                                echo "<tr>";
                                                echo "<td scope=\"row\">".$index."</td>";
                                                echo "<td>".$row['pr_name']."</td>";
                                                echo "<td>".$row['pr_id']."</td>";
                                                echo "<td>".$row['qty']."</td>";
                                                echo "<td> Rp. ".$row['price']."</td>";
                                                echo "<td>";
                                                echo "      <a href='#' data-t_id='".$row['t_id']."' data-id='".$row['ps_id']."' data-name='".$row['pr_name']."' class='sweet-confirm'><i class='mdi mdi-delete'></i></a>";
                                                echo "</td>";
                                                echo "</tr>";
                                                $index++;
                                            }
                                        ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="4" class="color-primary">Total Harga</td>
                                            <?php echo "<td colspan=\"2\" class='text-left color-primary'> Rp. " . $transaction['total_price'] . "</td>"; ?>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Page Content -->
        </div>
        <!-- End Container fluid  -->
        <?php } ?>
        <?php echo $footer; ?>
    </div>

    <div style="display: none">
        <form id="delete-form" method=""></form>
    </div>

</div>

</body>
<?php echo $js; ?>
<!-- Delete alert -->
<script src="<?php echo base_url('assets/js/lib/sweetalert/sweetalert.min.js'); ?>"></script>
<!-- Modified buttons -->
<script>
    $(function () {
        // $('#id_pelanggan').val('<?php// echo $transaction['mem_id']; ?>').trigger('change');
        $('.select-search').select2();

        $('#tableBarang tbody').on('click', '.sweet-confirm', function (e) {
            e.preventDefault();
            var t_id = $(this).data('t_id');
            var id = $(this).data('id');
            var name = $(this).data('name');
            swal({
                    title: "Konfirmasi",
                    text: "Apakah Anda yakin akan menghapus " + name + "?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ya",
                    cancelButtonText: "Tidak",
                    closeOnConfirm: true
                },
                function(){
                    $('#delete-form').attr('action', '<?php echo site_url('penjualan/deleteBarang/'); ?>' + id + '/' + t_id).submit();
                });
        });

        $('#btnID').on('click', '.sweet-confirm', function (e) {
            e.preventDefault();
            var t_id = $(this).data('t_id');
            swal({
                    title: "Konfirmasi",
                    text: "Apakah Anda yakin ingin membatalkan transaksi ?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ya",
                    cancelButtonText: "Tidak",
                    closeOnConfirm: true
                },
                function(){
                    $('#delete-form').attr('action', '<?php site_url("penjualan/menuKasir") ?>').submit();
                });
        });
    })
</script>
<script src="<?php echo base_url('/assets/js/lib/bootstrap/js/bootstrap_select.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/bootstrap/js/select2.min.js'); ?>"></script>
</html>

