<?php

defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ARUNA</title>

    <?php echo $css; ?>
</head>
<body class="fix-header fix-sidebar">
<?php echo $preloader; ?>

<div id="main-wrapper">
    <?php  echo $layout; ?>

    <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text">Transaksi</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Manajemen Penjualan</li>
                    <li class="breadcrumb-item">Menu Kasir</li>
                    <li class="breadcrumb-item active">Transaksi</li>
                </ol>
            </div>
        </div>
        <!-- End Bread crumb -->

         <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-title text-center">
                                <h2>Transaksi Berhasil Ditambahkan</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Page Content -->
        </div>

        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="post" enctype="multipart/form-data" class="form-horizontal">
                                <div class="form-group row">
                                    <label class="control-label col-md-2">Tanggal</label>
                                    <div class="col-md-6">
                                        <p class="m-t-12 form-control-static"><?php echo $tanggal; ?></p>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-md-2">ID Transaksi</label>
                                    <div class="col-md-6">
                                        <p class="m-t-12 form-control-static"><?php echo $transaction['t_id'];  ?></p>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-md-2">ID Pelanggan</label>
                                    <div class="col-md-6">
                                        <p class="m-t-12 form-control-static"><?php echo $transaction['mem_id'];  ?></p>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Nama Barang</th>
                                                <th>ID Barang</th>
                                                <th>Jumlah</th>
                                                <th>Harga</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                foreach ($product_sale as $row) {
                                                    echo "<tr>";
                                                    echo "<td>".$row['pr_name']."</td>";
                                                    echo "<td>".$row['pr_id']."</td>";
                                                    echo "<td>".$row['qty']."</td>";
                                                    echo "<td> Rp ".number_format($row['price'],2)."</td>";
                                                    echo "</tr>";
                                                }
                                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="3" class="color-danger">Diskon</td>
                                                <?php echo "<td class='text-right color-danger'> " . $transaction['disc'] . "% </td>"; ?>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="color-primary">Total Harga</td>
                                                <?php echo "<td class='text-right color-primary'> Rp " . number_format($transaction['total_price'],2) . "</td>"; ?>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <a href="<?php echo site_url('penjualan/menuKasir'); ?>" class="btn btn-info">OK</a>
                    </div>
                </div>
            </div>
            <!-- End Page Content -->
        </div>
        <!-- End Container fluid  -->
        <?php echo $footer; ?>
    </div>

</div>

</body>
<?php echo $js; ?>
</html>

