<?php

defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ARUNA</title>

    <?php echo $css; ?>
</head>
<body class="fix-header fix-sidebar">
<?php echo $preloader; ?>

<div id="main-wrapper">
    <?php echo $layout;?>

    <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text">Daftar Transaksi Penjualan</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Manajemen Penjualan</li>
                    <li class="breadcrumb-item active">Daftar Transaksi Penjualan</li>
                </ol>
            </div>
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div style="display: none">
                                <a href="#" class="btn btn-primary"><i class="mdi mdi-plus-circle"></i> Tambah Transaksi</a>
                            </div>
                            <div class="table-responsive m-t-20">
                                <table id="penjualanTable" class="display nowrap table table-hover table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>ID Transaksi</th>
                                            <th>Nama Kasir</th>
                                            <th>Tanggal Transaksi</th>
                                            <th>Total Harga</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        foreach ($transaction as $row) {
                                            echo "<tr>";
                                            echo "<td>".$row['t_id']."</td>";
                                            echo "<td>".$row['em_name']."</td>";
                                            $date = date_format(date_create($row['date_transaction']), "d M Y");
                                            echo "<td>".$date."</td>";
                                            echo "<td> Rp ".number_format($row['total_price'],2)."</td>";
                                            echo "<td class='text-center'>";
                                            echo "      <a href='".site_url('penjualan/detailTransaksi/').$row['t_id']."'><i class='mdi mdi-eye'></i></a> ";
                                            echo "</td>";
                                            echo "</tr>";
                                        }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Page Content -->
        </div>
        <!-- End Container fluid  -->

        <?php echo $footer; ?>
    </div>

    <div style="display: none">
        <form id="delete-form" method="POST"></form>
    </div>

</div>

</body>
<?php echo $js; ?>
<!-- Datatable & buttons -->
<script src="<?php echo base_url('/assets/js/lib/datatables/datatables.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js'); ?>"></script>
<!-- Modified buttons -->
<script>
    $(document).ready(function () {
        $('#penjualanTable').DataTable({
            dom: 'Bfrtip',
            buttons: [{ //customized datatable button
                extend: "excel",
                text: "<i class='fa fa-table'></i>",
                exportOptions: {
                    columns: ":not(:last-child)"
                }
            }, {
                extend: "pdf",
                text: "<i class='fa fa-file-pdf-o'></i>",
                exportOptions: {
                    columns: ":not(:last-child)"
                }
            }, {
                extend: "print",
                text: "<i class='fa fa-print'></i>",
                exportOptions: {
                    columns: ":not(:last-child)"
                }
            }]
        });
    })
</script>
</html>