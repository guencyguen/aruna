<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 6/2/2018
 * Time: 12:24 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ARUNA</title>

    <link href="<?php echo base_url('assets/css/lib/sweetalert/sweetalert.css'); ?>" rel="stylesheet">
    <?php echo $css; ?>
</head>
<body class="fix-header fix-sidebar">
<?php echo $preloader; ?>

<div id="main-wrapper">
    <?php echo $layout;?>

    <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text">Menu Kasir</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Manajemen Penjualan</li>
                    <li class="breadcrumb-item active">Menu Kasir</li>
                </ol>
            </div>
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="post" enctype="multipart/form-data" class="form-horizontal">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="control-label col-md-2">Tanggal</label>
                                            <div class="col-md-6">
                                                <p class="m-t-12 form-control-static"><?php echo $tanggal; ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label col-md-2">ID Pelanggan</label>
                                            <div class="col-md-3">
                                                <select name="id_pelanggan" id="id_pelanggan" class="form-control select-search" required>
                                                <option value="" selected disabled>Pilih ID...</option>
                                                <?php
                                                    foreach ($member as $row) {
                                                        echo "<option value='" . $row['mem_id'] . "'>" . $row['mem_id'] . "</option>";
                                                    }
                                                ?>
                                            </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label col-md-2">Diskon</label>
                                            <div class="col-md-2">
                                                <input type="number" name="diskon" id="diskon" class="form-control" min="0" required>
                                            </div>
                                            <div class="col-md-1">
                                                <label class="control-label col-md-2">%</label>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label col-md-2">Barang:</label>
                                        </div>
                                        <div class="row m-l-15">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">ID Barang</label>
                                                    <select name="barang[]" id="barang" class="form-control select-search" required>
                                                        <option value="" selected disabled>Pilih ID...</option>
                                                        <?php
                                                        foreach ($product as $row) {
                                                            echo "<option value='".$row['pr_id']."'>"
                                                                .$row['pr_id']."</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label">Jumlah</label>
                                                    <input type="number" name="qty[]" id="qty" class="form-control" min="0" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="moreItem"></div>
                                    </div>
                                    <div class="col-md-12 m-l-25">
                                        <div class="form-group">
                                            <button type="button" id="addItem" class="btn btn-outline-primary btn-sm"><i class="mdi mdi-plus"></i></button>
                                            <button type="button" id="removeItem" class="btn btn-sm btn-outline-danger"><i class="mdi mdi-minus"></i></button>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group" id="btnID">
                                            <button type="submit" class="btn btn-info">Tambah</button>
                                            <a href="#" class="btn btn-danger sweet-confirm">Batal</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Page Content -->
        </div>
        <!-- End Container fluid  -->

        <?php echo $footer; ?>
    </div>

    <div style="display: none">
        <form id="delete-form" method=""></form>
    </div>

</div>

</body>
<?php echo $js; ?>
<!-- Batal alert -->
<script src="<?php echo base_url('assets/js/lib/sweetalert/sweetalert.min.js'); ?>"></script>

<script>
    $(function () {
        $('#addItem').click(function () {
            var script = '<?php foreach ($product as $row) { echo "<option value=\'".$row['pr_id']."\'>".$row['pr_id']."</option>";}?>';
            $('.moreItem').append(
                '<div class="row m-l-15 new">'+
                '    <div class="col-md-6">'+
                '        <div class="form-group">'+
                '            <select name="barang[]" id="barang" class="form-control select-search" required>'+
                '                <option value="" selected disabled>Pilih barang...</option>' +
                                    script +
                '            </select>'+
                '        </div>'+
                '    </div>'+
                '    <div class="col-md-3">'+
                '        <div class="form-group">'+
                '            <input type="number" name="qty[]" id="qty" class="form-control" min="0" required>'+
                '        </div>'+
                '    </div>'+
                '</div>'
            );
            $('.select-search').select2();
        });
        $('#removeItem').click(function () {
            $('.new:last').remove();
        });

        $('.select-search').select2();

        $('#btnID').on('click', '.sweet-confirm', function (e) {
            e.preventDefault();
            swal({
                    title: "Konfirmasi",
                    text: "Apakah Anda yakin ingin membatalkan transaksi ?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ya",
                    cancelButtonText: "Tidak",
                    closeOnConfirm: true
                },
                function(){
                    $('#delete-form').attr('action', '<?php echo site_url('penjualan/menuKasir'); ?>').submit();
                });
        });
    })
</script>
<script src="<?php echo base_url('/assets/js/lib/bootstrap/js/bootstrap_select.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/bootstrap/js/select2.min.js'); ?>"></script>
</html>