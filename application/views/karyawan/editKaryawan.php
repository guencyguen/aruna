<?php

defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ARUNA</title>
    <?php echo $css; ?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('/assets/js/lib/pickers/jquery.datetimepicker.css'); ?>">
</head>
<body class="fix-header fix-sidebar">
<?php echo $preloader; ?>

<div id="main-wrapper">
    <?php echo $layout;?>

    <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text">Edit Karyawan #<?php echo $karyawan['em_id']; ?></h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Manajemen Karyawan</li>
                    <li class="breadcrumb-item">Daftar Karyawan</li>
                    <li class="breadcrumb-item active">Edit Karyawan</li>
                </ol>
            </div>
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form role="form" method="post" enctype="multipart/form-data" action="<?php echo site_url('karyawan/edit/').$karyawan['em_id']; ?>">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Nama</label>
                                            <input name="nama" id="nama" type="text" maxlength="50" class="form-control" value="<?php echo $karyawan['em_name']; ?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Tanggal Lahir</label>
                                            <div class='input-group'>
                                                <input type='text' class="form-control" name="tanggalLahir" id="tanggalLahir" value="<?php echo $karyawan['date_birth']; ?>" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Jenis Kelamin</label>
                                            <div>
                                                <label class="m-radio m-radio--success">
                                                    <input type="radio" name="gender" value="Wanita" <?php if($karyawan['em_gender'] == "Wanita") echo "checked";?>>
                                                    Wanita
                                                    <span></span>
                                                </label>
                                            </div>
                                            <div>
                                                <label class="m-radio m-radio--success">
                                                    <input type="radio" name="gender" value="Pria" <?php if($karyawan['em_gender'] == "Pria") echo "checked";?>>
                                                    Pria
                                                    <span></span>
                                                </label>
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label>Tanggal Mulai Kerja</label>
                                            <div class='input-group'>
                                                <input type='text' class="form-control" name="tanggalMulaiKerja" id="tanggalMulaiKerja" value="<?php echo $karyawan['date_entry']; ?>" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Jabatan</label>
                                            <select name="jabatan" id="jabatan" class="form-control">
                                                <?php foreach($jabatan as $row): ?>
                                                    <option value="<?php echo $row['id']; ?>" <?php if($row['id'] == $karyawan['job_position']) echo 'selected'; ?>><?php echo $row['name']; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat</label>
                                            <input name="alamat" id="alamat" type="text" class="form-control" maxlength="120" value="<?php echo $karyawan['em_address']; ?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label>E-mail</label>
                                            <input name="email" id="email" type="email" class="form-control" value="<?php echo $karyawan['em_email']; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>No. Telepon</label>
                                            <input name="telepon" id="telepon" type="number" class="form-control" min="0" value="<?php echo $karyawan['em_phone_number']; ?>" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-info">Update</button>
                                            <a href="<?php echo site_url('karyawan/index'); ?>" class="btn btn-danger">Batal</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Page Content -->
        </div>
        <!-- End Container fluid  -->

        <?php echo $footer; ?>
    </div>

</div>

</body>
<?php echo $js; ?>
<script src="<?php echo base_url('/assets/js/lib/pickers/jquery.datetimepicker.full.js'); ?>"></script>
<script>
    $('#tanggalLahir').datetimepicker({
        timepicker : false,
        format:'Y-m-d'
    });
    $('#tanggalMulaiKerja').datetimepicker({
        timepicker : false,
        format:'Y-m-d'
    });
</script>
</html>
