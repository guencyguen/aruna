<?php

defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ARUNA</title>
    <?php echo $css; ?>
</head>
<body class="fix-header fix-sidebar">
<?php echo $preloader; ?>

<div id="main-wrapper">
    <?php echo $layout;?>

    <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text">Tambah Karyawan Baru</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Manajemen Karyawan</li>
                    <li class="breadcrumb-item">Daftar Karyawan</li>
                    <li class="breadcrumb-item active">Tambah Karyawan</li>
                </ol>
            </div>
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="<?php echo site_url('karyawan/addKaryawan'); ?>" method="post" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Nama Karyawan</label>
                                            <input name="nama" id="nama" type="text" maxlength="50" class="form-control" value="<?php echo set_value('nama') ?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Posisi</label>
                                            <select name="posisi" class="form-control" required>
                                                <option value="2" <?php echo set_select('posisi', '2'); ?>>Kasir</option>
                                                <option value="3" <?php echo set_select('posisi', '3'); ?>>Warehouse</option>
                                                <option value="4" <?php echo set_select('posisi', '4'); ?>>Purchasing</option>
                                                <option value="1" <?php echo set_select('posisi', '1'); ?>>Manajer</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Jenis Kelamin</label>
                                            <select name="gender" class="form-control" required>
                                                <option value="Pria" <?php echo set_select('gender', 'male'); ?>>Pria</option>
                                                <option value="Wanita" <?php echo set_select('gender', 'female'); ?>>Wanita</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat</label>
                                            <input name="alamat" id="alamat" type="text" class="form-control" maxlength="120" value="<?php echo set_value('alamat'); ?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Tanggal Lahir</label>
                                            <input name="ttl" id="ttl" type="date" class="form-control" value="<?php echo set_value('ttl'); ?>" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>E-mail</label>
                                            <input name="email" id="email" type="email" class="form-control" value="<?php echo set_value('email'); ?>">
                                            <?php echo form_error('email', '<span class="text-danger">', '</span>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Username</label>
                                            <input name="username" id="username" type="text" maxlength="50" class="form-control" value="<?php echo set_value('username') ?>" required>
                                            <?php echo form_error('username', '<span class="text-danger">', '</span>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Password</label>
                                            <input name="password" id="password" type="password" class="form-control" value="<?php echo set_value('password') ?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Nomor Telepon</label>
                                            <input name="no_telp" id="poin" type="number" class="form-control" min="0" value="<?php echo set_value('no_telp'); ?>" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-info">Tambah</button>
                                            <a href="<?php echo site_url('karyawan/index'); ?>" class="btn btn-danger">Batal</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Page Content -->
        </div>
        <!-- End Container fluid  -->

        <?php echo $footer; ?>
    </div>

</div>

</body>
<?php echo $js; ?>
</html>