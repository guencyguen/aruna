<?php

defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ARUNA</title>
    <?php echo $css; ?>
</head>
<body class="fix-header fix-sidebar">
<?php echo $preloader; ?>

<div id="main-wrapper">
    <?php echo $layout;?>

    <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text">Detail Karyawan #<?php echo $karyawan['em_id']; ?></h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Manajemen Karyawan</li>
                    <li class="breadcrumb-item">Daftar Karyawan</li>
                    <li class="breadcrumb-item active">Detail Karyawan</li>
                </ol>
            </div>
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="post" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label">Nama Karyawan</label>
                                            <div class="col-sm-10">
                                                <p class="form-control-static"><?php echo $karyawan['em_name']; ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Tanggal Lahir</label>
                                            <div class="col-sm-10">
                                                <p class="form-control-static"><?php echo $karyawan['date_birth'] ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Jenis Kelamin</label>
                                            <div class="col-sm-10">
                                                <p class="form-control-static"><?php echo $karyawan['em_gender']; ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Tanggal Mulai Kerja</label>
                                            <div class="col-sm-10">
                                                <p class="form-control-static"><?php echo $karyawan['tanggal_masuk']; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label">Jabatan</label>
                                            <div class="col-sm-10">
                                                <p class="form-control-static"><?php echo $jabatan[$karyawan['job_position']]; ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Alamat</label>
                                            <div class="col-sm-10">
                                                <p class="form-control-static"><?php echo $karyawan['em_address']; ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">E-mail</label>
                                            <div class="col-sm-10">
                                                <p class="form-control-static"><?php echo $karyawan['em_email']; ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Username</label>
                                            <div class="col-sm-10">
                                                <p class="form-control-static"><?php echo $karyawan['username']; ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">No. Telepon</label>
                                            <div class="col-sm-10">
                                                <p class="form-control-static"><?php echo $karyawan['em_phone_number']; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <a href="<?php echo site_url('karyawan/edit/').$karyawan['em_id']; ?>" class="btn btn-info">Edit</a>
                                            <a href="<?php echo site_url('karyawan/index'); ?>" class="btn btn-danger">Kembali</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Page Content -->
        </div>
        <!-- End Container fluid  -->

        <?php echo $footer; ?>
    </div>

</div>

</body>
<?php echo $js; ?>
</html>