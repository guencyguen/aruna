<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 6/1/2018
 * Time: 3:36 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ARUNA</title>

    <?php echo $css; ?>
</head>
<body class="fix-header fix-sidebar">
<?php echo $preloader; ?>

<div id="main-wrapper">
    <?php echo $layout;?>

    <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text">Edit Order <?php echo $order[0]['o_id']; ?></h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Manajemen Barang</li>
                    <li class="breadcrumb-item">Order Barang</li>
                    <li class="breadcrumb-item active">Edit Order</li>
                </ol>
            </div>
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="post" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="control-label col-md-2">Dipesan Oleh</label>
                                            <div class="col-md-6">
                                                <p class="form-control-static"><?php echo $order[0]['em_name'] ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label col-md-2">Tanggal Pesan</label>
                                            <div class="col-md-6">
                                                <p class="form-control-static"><?php echo date_format(new DateTime($order[0]['o_date']), 'd M Y') ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label col-md-2">Barang:</label>
                                        </div>
                                        <div class="row m-l-15">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Nama Barang & Supplier</label>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label">Jumlah</label>
                                                </div>
                                            </div>
                                        </div>
                                        <?php foreach ($order as $item) { ?>
                                        <div class="row m-l-15">
                                            <div class="col-md-6">
                                                <div class="form-grop">
                                                    <select name="barang[]" id="barang" class="form-control select-search" required>
                                                        <?php
                                                        foreach ($products as $row) {
                                                            echo "<option value='".$row['pr_id'];
                                                                if ($row['pr_id'] == $item['p_id']) echo "' selected";
                                                                else echo "'";
                                                            echo ">".$row['pr_name']." (".$row['supplier_name'].")</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <input type="number" name="qty[]" id="qty" class="form-control" min="0" value="<?php echo $item['qty'] ?>" required>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-info">Update</button>
                                            <a href="<?php echo site_url('orderbarang/index'); ?>" class="btn btn-danger">Batal</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Page Content -->
        </div>
        <!-- End Container fluid  -->

        <?php echo $footer; ?>
    </div>

</div>

</body>
<?php echo $js; ?>
<script>
    $(function () {
        $('.select-search').select2();
    })
</script>
<script src="<?php echo base_url('/assets/js/lib/bootstrap/js/bootstrap_select.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/lib/bootstrap/js/select2.min.js'); ?>"></script>
</html>