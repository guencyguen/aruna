<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 6/1/2018
 * Time: 11:57 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class SupplierModel extends CI_Model
{
    public function getAll()
    {
        return $this->db->get('supplier')->result_array();
    }

    public function view(){
        return $this->db->get('supplier')->result();
    }

    public function getSpecified($id)
    {
        return $this->db->get_where('supplier', array('s_id' => $id))->row_array();
    }

    public function insert($data)
    {
        $this->db->insert('supplier', $data);
    }

    public function delete($id)
    {
        $this->db->delete('supplier', array('s_id' => $id));
    }

    public function update($data, $id)
    {
        $this->db->update('supplier', $data, array('s_id' => $id));
    }
    
    public function insert_multiple($data){
        $this->db->insert_batch('supplier', $data);
    }

}