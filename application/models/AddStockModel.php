<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AddStockModel extends CI_Model
{
    public function getAll()
    {
        return $this->db->select('o_id, addstock.em_id as em_id, count(po) as po, count(p_id) as total_item, sum(total_price) as total_biaya, employee.em_name as employee', FALSE)
            ->from('addstock')->join('employee', 'employee.em_id = addstock.em_id')
            ->group_by('o_id')->get()->result_array();
    }

    public function getSpecified($id)
    {
        return $this->db->select('o_id, addstock.em_id as em_id, count(po) as po, o_date, addstock.em_id as em_id, employee.em_name as em_name, p_id, product.pr_name as pr_name, qty', FALSE)
            ->from('addstock')->join('employee', 'employee.em_id = addstock.em_id')->join('product', 'product.pr_id = addstock.p_id')
            ->where('o_id', $id)->get()->result_array();
    }

    public function getSpecifiedWithoutPO($id)
    {
        return $this->db->select('id, o_id, addstock.em_id as em_id, o_date, addstock.em_id as em_id, employee.em_name as em_name, p_id, product.pr_name as pr_name, qty', FALSE)
            ->from('addstock')->join('employee', 'employee.em_id = addstock.em_id')->join('product', 'product.pr_id = addstock.p_id')
            ->where('o_id', $id)->get()->result_array();
    }

    public function getDetailWithSupplier($id)
    {
        return $this->db->select('id, o_id, po, o_date, supplier.s_nama as s_name, product.pr_name as pr_name, price, total_price, qty', FALSE)
            ->from('addstock')->join('product', 'product.pr_id = addstock.p_id')
            ->join('supplier', 'supplier.s_id = addstock.s_id', 'left')
            ->where('o_id', $id)->get()->result_array();
    }

    public function getOrderId()
    {
        return $this->db->select('o_id')->from('addstock')->order_by('o_id', 'DESC')->get()->result_array();
    }

    public function insert($data)
    {
        $this->db->insert('addstock', $data);
    }

    public function delete($id)
    {
        $this->db->delete('addstock', array('o_id' => $id));
    }

    public function deleteItem($o_id, $p_id)
    {
        $this->db->where(array('o_id' => $o_id, 'p_id' => $p_id))->delete('addstock');
    }

    public function deleteFromId($id)
    {
        $this->db->delete('addstock', array('id' => $id));
    }

    public function update($data, $id)
    {
        $this->db->update('addstock', $data, array('id' => $id));
    }


    // PURCHASE ORDER

    public function getAllPO()
    {
        return $this->db->select('o_id, count(distinct po) as po, count(distinct s_id) as s_id, count(p_id) as total_item, sum(total_price) as total_biaya, employee.em_name as employee', FALSE)
            ->from('addstock')->join('employee', 'employee.em_id = addstock.em_id')
            ->group_by('o_id')->get()->result_array();
    }

    public function updatePO($data, $o_id, $s_id)
    {
        $this->db->update('addstock', $data, array('o_id' => $o_id, 's_id' => $s_id));
    }

    public function getOrderPO($id)
    {
        return $this->db->select('o_id, po, addstock.s_id, supplier.s_nama as s_name, count(p_id) as total_item, sum(total_price) as total_price', FALSE)
            ->from('addstock')->join('supplier', 'supplier.s_id = addstock.s_id')
            ->where('o_id', $id)->group_by('addstock.s_id')->get()->result_array();
    }

    public function getFromSupplier($o_id, $s_id)
    {
        return $this->db->select('p_id, qty')->from('addstock')->where(array('o_id' => $o_id, 's_id' => $s_id))->get()->result_array();
    }

    public function getDetailPO($id)
    {
        return $this->db->select('o_id, po, qty, addstock.p_id as p_id, po_date, addstock.s_id, price, total_price, product.pr_name as pr_name,
                    employee.em_name as em_name, addstock.em_id, employee.em_phone_number as em_phone_number')
            ->from('addstock')->join('product', 'product.pr_id = addstock.p_id')
            ->join('employee', 'employee.em_id = addstock.em_id')
            ->where('po', $id)->get()->result_array();
    }

    public function cancelPO($id)
    {
        $data = Array ('po' => null);
        $this->db->update('addstock', $data, array('po' => $id));
    }

    public function getEmployeePos($id)
    {
        return $this->db->select('job_position.name as pos')->from('job_position')
            ->join('employee', 'employee.job_position = job_position.id')
            ->where('employee.em_id', $id)->get()->result_array();
    }
}
?>