<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PelangganModel extends CI_Model
{
    public function getAll()
    {
        return $this->db->get('member')->result_array();
    }

    public function getAllWithoutNN()
    {
        return $this->db->select('*')->from('member')->where('mem_id !=', '239999')->get()->result_array();
    }

    public function getSpecified($id)
    {
        return $this->db->get_where('member', array('mem_id' => $id))->row_array();
    }

    public function insert($data)
    {
        $this->db->insert('member', $data);
    }

    public function delete($id)
    {
        $this->db->delete('member', array('mem_id' => $id));
    }

    public function update($data, $id)
    {
        $this->db->update('member', $data, array('mem_id' => $id));
    }

    public function getBirthdayMember()
    {
        return $this->db->select('*')->from('member')
            ->where('day(mem_date_birth) =', 'day(CURRENT_DATE)')
            ->where('month(mem_date_birth) =', 'month(CURRENT_DATE)')
            ->get()->result_array();
    }
}