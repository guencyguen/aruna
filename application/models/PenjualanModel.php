<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PenjualanModel extends CI_Model
{
    public function getAllMember()
    {
        return $this->db->get('member')->result_array();
    }

    public function getAllProduct()
    {
        return $this->db->get('product')->result_array();
    }

    public function getAllTransaction()
    {
        $this->db->select('t_id, date_transaction, mem_id, total_price, total_profit, transaction.em_id, em_name');
        $this->db->from('transaction');
        $this->db->join('employee', 'employee.em_id = transaction.em_id');
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function getListTransaction($id)
    {
        $this->db->select('t_id, date_transaction, mem_id, total_price, total_profit, transaction.em_id, em_name, disc');
        $this->db->from('transaction');
        $this->db->join('employee', 'employee.em_id = transaction.em_id');
        $this->db->where('t_id', $id);
        $query = $this->db->get()->row_array();
        return $query;
    }

    public function getListProductSale($id)
    {
        $this->db->select('ps_id, product_sale.pr_id, qty, price, t_id, pr_name');
        $this->db->from('product_sale');
        $this->db->join('product', 'product.pr_id = product_sale.pr_id');
        $this->db->where('t_id', $id);
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function getSpecifiedProduct($id)
    {
        return $this->db->get_where('product', array('pr_id' => $id))->row_array();
    }

    public function getSpecifiedProductSale($id)
    {
        return $this->db->get_where('product_sale', array('t_id' => $id))->row_array();
    }

    public function getSpecifiedProductSale_($id)
    {
        return $this->db->get_where('product_sale', array('ps_id' => $id))->row_array();
    }

    public function getSpecifiedTransaction($id)
    {
        return $this->db->get_where('transaction', array('t_id' => $id))->row_array();
    }

    public function getSpecifiedMember($id)
    {
        return $this->db->get_where('member', array('mem_id' => $id))->row_array();
    }

    public function insert_transaction($data)
    {
        $this->db->insert('transaction', $data);
    }

    public function insert_product_sale($data)
    {
        $this->db->insert('product_sale', $data);
    }

    public function deleteBarang($ps_id)
    {
        $this->db->delete('product_sale', array('ps_id' => $ps_id));
    }

    public function deleteTransaksi($t_id)
    {
        $this->db->delete('transaction', array('t_id' => $t_id));
    }

    public function deleteAllBarang($t_id)
    {
        $this->db->delete('product_sale', array('t_id' => $t_id));
    }

    public function updateProduct($data, $id)
    {
        $this->db->update('product', $data, array('pr_id' => $id));
    }

    public function update_transaction($data, $id)
    {
        $this->db->update('transaction', $data, array('t_id' => $id));
    }

    public function updateMember($data, $id)
    {
        $this->db->update('member', $data, array('mem_id' => $id));
    }
}