<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 6/1/2018
 * Time: 11:05 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductModel extends CI_Model
{
    public function getAll()
    {
        return $this->db->get('product')->result_array();
    }

    public function getAllInventory()
    {
        return $this->db->select('*')->from('product')->where('pr_inventory >', 0)->get()->result_array();
    }

    public function view(){
        return $this->db->get('product')->result();
    }

    public function getAllWithSupplier()
    {
        return $this->db->select('pr_id, pr_name, supplier.s_nama as supplier_name', FALSE)
            ->from('product')->join('supplier', 'supplier.s_id = product.s_id')
            ->get()->result_array();
    }

    public function getSpecified($id)
    {
        return $this->db->get_where('product', array('pr_id' => $id))->row_array();
    }

    public function getProductFrom($supplier)
    {
        return $this->db->get_where('product', array('s_id' => $supplier))->result_array();
    }

    public function insert($data)
    {
        $this->db->insert('product', $data);
    }

    public function delete($id)
    {
        $this->db->delete('product', array('pr_id' => $id));
    }

    public function update($data, $id)
    {
        $this->db->update('product', $data, array('pr_id' => $id));
    }
    
    public function insert_multiple($data)
    {
        $this->db->insert_batch('product', $data);
    }

    public function addStok($pr_id, $qty)
    {
        $this->db->query('UPDATE product SET pr_inventory = pr_inventory + '.$qty.' WHERE pr_id = '.$pr_id);
    }

    public function minusStok($pr_id, $qty)
    {
        $this->db->query('UPDATE product SET pr_inventory = pr_inventory - '.$qty.' WHERE pr_id = '.$pr_id);
    }

}