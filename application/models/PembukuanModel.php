<?php
/**
 * Created by Hauw.
 * User: anitut
 * Date: 6/26/2018
 * Time: 18:05 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class PembukuanModel extends CI_Model
{
    public function pengeluaran()
    {
        $this->db->select('id, po, o_id, po_date, addstock.s_id, s_nama, qty, sum(total_price) as total_price');
        $this->db->from('addstock');
        $this->db->join('supplier', 'supplier.s_id = addstock.s_id');
        $this->db->group_by('po');
        $this->db->where('po !=', null);
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function pemasukan()
    {
        $this->db->select('transaction.t_id as t_id, date_transaction, mem_id, total_price, total_profit, transaction.em_id, em_name, sum(qty) as qty');
        $this->db->from('transaction');
        $this->db->join('employee', 'employee.em_id = transaction.em_id');
        $this->db->join('product_sale', 'product_sale.t_id = transaction.t_id');
        $this->db->group_by('t_id');
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function total_pemasukan()
    {
        $this->db->select_sum('total_price');
        $this->db->select_sum('total_profit');
        $this->db->from('transaction');        
        $query = $this->db->get()->row_array();
        return $query;
    }

    public function total_profit()
    {
        $this->db->select_sum('total_profit');
        $this->db->from('transaction');
        $query = $this->db->get()->row_array();
        return $query;
    }

    public function total_pengeluaran()
    {
        $this->db->select_sum('total_price');
        $this->db->from('addstock');
        $this->db->where('po !=', null);
        $query = $this->db->get()->row_array();
        return $query;
    }

}