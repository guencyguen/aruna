<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ACL extends CI_Model
{
    public function allow($controller_name)
    {
        $current_controller = $this->db->get_where('management_menu', array('name' => strtolower($controller_name)));
        if($current_controller->num_rows() > 0)
        {
            $acl = $this->db->get_where(
                'acl',
                array(
                    'job_position_id' => $this->session->job_position_id,
                    'management_menu_id' => $current_controller->row()->id
                )
            );
            if($acl->num_rows() == 0) return 'NOT ALLOWED';
            else return 'ALLOWED';
        }
        else return 'CONTROLLER NOT FOUND';
    }
}

?>