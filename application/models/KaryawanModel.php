<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KaryawanModel extends CI_Model
{
    public function getAll()
    {
        return $this->db->get('employee')->result_array();
    }

    public function getSpecified($id)
    {
        return $this->db->get_where('employee', array('em_id' => $id))->row_array();
    }

    public function insert($data)
    {
        $this->db->insert('employee', $data);
    }

    public function delete($id)
    {
        $this->db->delete('employee', array('em_id' => $id));
    }

    public function update($data, $id)
    {
        $this->db->update('employee', $data, array('em_id' => $id));
    }
}
?>