-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 09, 2018 at 03:51 AM
-- Server version: 5.7.13-log
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aruna_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `acl`
--

CREATE TABLE `acl` (
  `job_position_id` int(11) NOT NULL,
  `management_menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `acl`
--

INSERT INTO `acl` (`job_position_id`, `management_menu_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(2, 3),
(2, 5),
(3, 1),
(3, 2),
(4, 2),
(1, 6),
(3, 6),
(1, 7),
(4, 7),
(1, 8),
(1, 9),
(1, 10),
(2, 9),
(3, 10),
(4, 9),
(4, 10),
(1, 11);

-- --------------------------------------------------------

--
-- Table structure for table `addstock`
--

CREATE TABLE `addstock` (
  `id` int(6) NOT NULL,
  `o_id` varchar(25) NOT NULL,
  `po` varchar(25) DEFAULT NULL,
  `p_id` int(6) NOT NULL,
  `em_id` int(6) NOT NULL,
  `s_id` int(6) NOT NULL,
  `o_date` date DEFAULT NULL,
  `po_date` date DEFAULT NULL,
  `price` int(9) NOT NULL,
  `qty` int(4) NOT NULL,
  `total_price` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `em_id` int(6) NOT NULL,
  `em_name` varchar(50) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(300) NOT NULL,
  `em_gender` varchar(7) NOT NULL,
  `date_birth` date NOT NULL,
  `em_address` varchar(255) NOT NULL,
  `em_phone_number` varchar(12) DEFAULT NULL,
  `em_email` varchar(100) DEFAULT NULL,
  `date_entry` date NOT NULL,
  `job_position` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`em_id`, `em_name`, `username`, `password`, `em_gender`, `date_birth`, `em_address`, `em_phone_number`, `em_email`, `date_entry`, `job_position`) VALUES
(110001, 'Bagyo Aruna', 'arunaadmin', '$2y$10$TVdGmcBTfcvnSb8Ylle9A.x4tN1H.iybh0dtfZDv/YkvVhmfoCuIG', 'Pria', '2091-05-26', 'Jl. Sultan Agung 1 no.16, Cibodas Baru, Cibodas, Kota Tangerang, Banten 15138', '089616542378', NULL, '2005-05-07', 1),
(110002, 'Iwan Ganda Saputra', 'iwangs123', '$2y$10$zd7jghJoz3h6uZD36Y04Euom7/RLDHcb90nbIDonVbVYiXH1VtNRS', 'Pria', '1974-12-19', 'Jl. Cendana Raya no.77, Kuta Jaya, PasarKemis, Tangerang, Banten 15560', '089535624895', '', '2016-10-19', 4),
(110003, 'Leviana dharmadi', 'leleviana', '$2y$10$UcMquuZ1w.vQdFdq1DvDy.23eWQAEo/2bVVcF5DGZuMxNJmhV.zji', 'Wanita', '1986-04-16', 'Jl. Pinisi Indah 1 No.9, Kapuk Muara, Penjaringan, Kota Jkt Utara, Daerah Khusus Ibukota Jakarta 14460', '081316459783', '', '2013-02-09', 3),
(110004, 'Wahid Mukhammad Nur', 'wahidmnur', '$2y$10$oXRPhWYxcTSl/D2C9j0E5uu/p6.4BU.zu7HjefXGwXA4z2j/JAQWK', 'Pria', '1987-12-12', 'Jl. Taman Suropati, RT.5/RW.5, Menteng, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10310', '081346545645', NULL, '2012-12-25', 2),
(110006, 'Alexandro', 'alexandro', '$2y$10$c/012d0U8gjsALFjd8oV7.IcYOYL30hGJy8QKetaviTpvNRPsAOpq', 'Pria', '1992-03-16', 'Jl. Palem Indah No.7', '089526453126', NULL, '2012-04-10', 3),
(110007, 'Rian Ananda Putra', 'rianpuput', '$2y$10$8ljDa.5xM0ELHwEpWrafruttD12OC7Q8dPB7t7qn1KcPh2oDamdam', 'Pria', '1996-05-10', 'Jl. Al-Ikhlas No.59, Kota Tangerang Selatan', '08185467821', NULL, '2015-09-12', 3),
(110008, 'Ria Rizqina', 'riariaria', '$2y$10$iAlZhGGPXtUBOUNcivrUJ.gtt2GIr9IIRw0iGm8cUOsuV8DNNpu56', 'Wanita', '1982-09-06', 'Jl. Pisangan Raya No.42, Kota Tangerang Selatan', '085613459762', '', '2017-09-14', 2),
(110014, 'Uvuvwevwevwe Onyetenyevwe Ugwemubwem Osas', 'osasss', '$2y$10$d4Txth/xI80xqBYLNQhmiujJ80oh.DAtEog1dnkFYUkmKTZEKJ/T.', 'Pria', '1990-08-22', 'Taman Cibodas Tangerang', '08218439384', '', '2015-08-10', 4),
(110015, 'Dian Puspita', 'dpch', '$2y$10$d4Txth/xI80xqBYLNQhmiujJ80oh.DAtEog1dnkFYUkmKTZEKJ/T.', 'Wanita', '1997-11-08', 'Binong', '08123456789', 'dian.chandra@student.umn.ac.id', '2018-04-10', 4),
(110016, 'Christine Liviani', 'itin', '$2y$10$d4Txth/xI80xqBYLNQhmiujJ80oh.DAtEog1dnkFYUkmKTZEKJ/T.', 'Wanita', '1997-11-11', 'Gading Serpong', '082146834934', 'christine.liviani@student.umn.ac.id', '2018-06-13', 4),
(110017, 'Bayu Tirta', 'bayutirta', '$2y$10$d4Txth/xI80xqBYLNQhmiujJ80oh.DAtEog1dnkFYUkmKTZEKJ/T.', 'Pria', '1997-11-11', 'Bonang', '08123834723', 'bayutirta@gmail.com', '2018-06-25', 2);

-- --------------------------------------------------------

--
-- Table structure for table `job_position`
--

CREATE TABLE `job_position` (
  `id` int(2) NOT NULL,
  `name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_position`
--

INSERT INTO `job_position` (`id`, `name`) VALUES
(1, 'Manajer'),
(2, 'Kasir'),
(3, 'Warehouse'),
(4, 'Purchasing');

-- --------------------------------------------------------

--
-- Table structure for table `management_menu`
--

CREATE TABLE `management_menu` (
  `id` int(11) NOT NULL,
  `name` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `management_menu`
--

INSERT INTO `management_menu` (`id`, `name`) VALUES
(1, 'product'),
(2, 'supplier'),
(3, 'penjualan'),
(4, 'karyawan'),
(5, 'pelanggan'),
(6, 'orderbarang'),
(7, 'purchaseorder'),
(8, 'lihat_total_laba_bersih'),
(9, 'lihat_jumlah_transaksi_penjualan'),
(10, 'lihat_jumlah_po'),
(11, 'pembukuan');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `mem_id` int(6) NOT NULL,
  `mem_fullname` varchar(50) NOT NULL,
  `mem_address` varchar(255) NOT NULL,
  `mem_date_birth` date NOT NULL,
  `mem_gender` varchar(7) NOT NULL,
  `mem_email` varchar(100) DEFAULT NULL,
  `poin` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`mem_id`, `mem_fullname`, `mem_address`, `mem_date_birth`, `mem_gender`, `mem_email`, `poin`) VALUES
(130002, 'Intan Fadila Hidayat', 'jl. sultan agung 1 no 17', '1991-05-02', 'female', '', 389),
(130003, 'Lia Ayu Anggraeni', 'Jl. Pesar No.45, Binong', '1995-07-11', 'female', NULL, 705),
(130004, 'Daenuri', 'Jl. Pemuda block B no 3, Klp. Dua', '1996-09-21', 'male', NULL, 1281),
(130005, 'Gregorio', 'Jl. Syamsi Raya Blok B No.57', '1995-11-16', 'male', NULL, 46),
(130006, 'Mariani', 'Jl. Klp. Sawit V No.15', '1984-05-24', 'female', 'marmarif2015@gmail.com', 13),
(130007, 'Ray Guarchia', 'Jl. Taman Kenanga Blok A16 No.8, Poris Plawad Indah', '1992-10-10', 'male', NULL, 80),
(130008, 'Arita Wulandari', 'Polsek Cipondoh, Jalan K.H. Hasyim Ashari KM. 7, Cipondoh', '1998-09-21', 'female', NULL, 540),
(130009, 'Agnes Nadhita', 'Jl. Kp. Rw. Rotan 35-38, Selapajang Jaya, Neglasari', '1988-02-07', 'female', NULL, 135),
(130010, 'Cherina Apriyanti', 'Gg. Chandra, Kby. Lama Utara, Kota Jakarta Selatan', '1995-01-08', 'female', NULL, 26),
(130011, 'Patricia Sahira', 'Jl. Puri Utara IB, Sawah Baru, Kota Tangerang Selatan', '1993-01-13', 'female', NULL, 987),
(130017, 'Jennie Lim', 'Jl. Cisomewhere No. 25', '2001-06-09', 'female', '', 163),
(130018, 'Viyana Chita Silvera', 'Jl. Scientia Square Utara Boulevard', '1996-06-05', 'female', 'richanchan3@gmail.com', 0),
(130019, 'Lucas', 'Jl. Cisomewhere No. 25', '2001-06-09', 'male', '', 84),
(131111, 'N/N', '', '0000-00-00', '', '', 75);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `pr_id` int(6) NOT NULL,
  `s_id` int(6) NOT NULL,
  `pr_name` varchar(50) NOT NULL,
  `pr_stock` int(7) DEFAULT '0',
  `pr_inventory` int(7) DEFAULT '0',
  `pr_category` varchar(30) NOT NULL,
  `date_received` datetime DEFAULT NULL,
  `harga_jual` int(8) NOT NULL,
  `harga_beli` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`pr_id`, `s_id`, `pr_name`, `pr_stock`, `pr_inventory`, `pr_category`, `date_received`, `harga_jual`, `harga_beli`) VALUES
(140001, 150004, 'Bear brand 140ml', 1526, 0, 'Susu', '2018-05-25 12:13:20', 8350, 7500),
(140002, 150007, 'Lmen G Mass 225gr', 446, 0, 'Susu', '2018-05-11 06:35:12', 36500, 34200),
(140003, 150007, 'HiloTeen 250gr Coklat', 447, 0, 'Susu', '2018-05-17 19:24:06', 24320, 21650),
(140004, 150001, 'Kokokrunch 340gr coklat', 254, 0, 'Sereal', '2018-05-26 10:20:22', 35100, 33500),
(140005, 150001, 'Munchys Oat Krunch Kraker', 735, 0, 'Sereal', '2018-05-17 15:47:36', 38999, 36500),
(140006, 150001, 'Chacha 1kg peanut', 946, 0, 'Makanan ringan', '2018-04-27 12:09:22', 76000, 68000),
(140007, 150007, 'Beng beng drink 1 renceng', 3510, 0, 'Minuman seduh', '2018-05-25 05:53:20', 14875, 12000),
(140008, 150001, 'Astor Wafer Stick kaleng', 366, 0, 'Makanan ringan', '2018-05-17 16:18:33', 29500, 28000),
(140009, 150007, 'Dancow Balita 150 gr', 540, 0, 'Susu', '2018-05-17 16:18:33', 9750, 9000),
(140010, 150007, 'Nutrilon 2tb 200gr', 245, 0, 'Susu', '2018-05-17 16:18:33', 36000, 35000),
(140011, 150007, 'Dancow 400gr 1+', 812, 0, 'Susu', '2018-05-16 00:00:00', 29200, 25000),
(140012, 150007, 'SGM 0-6 Bln', 256, 0, 'Susu', '2018-05-14 07:30:06', 11200, 10000),
(140013, 150001, 'Tanggo wafer 800gr', 754, 0, 'Makanan ringan', '2018-05-17 15:06:24', 200000, 185000),
(140014, 150001, 'makaroni jamilee 500gr', 564, 0, 'Makanan ringan', '2018-05-31 00:00:00', 25000, 24000),
(140015, 150001, 'Khong Guan biskuit', 895, 0, 'Makanan ringan', '2018-05-31 00:00:00', 75000, 72000),
(140016, 150001, 'Briko wafer coklat', 134, 0, 'Makanan ringan', '2018-05-31 00:00:00', 50000, 48000),
(140017, 150001, 'chocolatos 1 box', 461, 0, 'Makanan ringan', '2018-05-31 00:00:00', 10000, 8400),
(140018, 150001, 'bengbeng 1 box', 1645, 0, 'Makanan ringan', '2018-05-31 00:00:00', 26900, 24500),
(140019, 150001, 'Go potato biskuit 20pcs', 500, 0, 'Makanan ringan', '2018-05-31 00:00:00', 9000, 8000),
(140020, 150001, 'Apollo Roka Wafer', 942, 0, 'Makanan ringan', '2018-05-31 00:00:00', 43500, 41999),
(140021, 150001, 'pop mie kuah pedes dower', 2645, 0, 'Makanan instan', '2018-05-31 00:00:00', 5000, 4300),
(140022, 150001, 'wafer coklat superstar', 742, 0, 'Makanan ringan', '2018-05-31 00:00:00', 11500, 10000),
(140023, 150001, 'better biskuit sandwich 10pcs', 620, 0, 'Makanan ringan', '2018-05-31 00:00:00', 9000, 8000),
(140024, 150005, 'Taro chiki 10 pcs', 1564, 0, 'Makanan ringan', '2018-05-31 00:00:00', 10000, 90000),
(140025, 150005, 'Biskuit oreo box 10x23gr', 240, 0, 'Makanan ringan', '2018-05-31 00:00:00', 18000, 16500),
(140026, 150004, 'fanta strawberry 330ml', 500, 0, 'Minuman', '2018-05-31 00:00:00', 5000, 4500),
(140027, 150005, 'biskuit roma 300gr', 120, 0, 'Makanan ringan', '2018-05-31 00:00:00', 6700, 6000),
(140028, 150005, 'miss peucun mix', 250, 0, 'Makanan ringan', '2018-05-31 00:00:00', 18130, 16000),
(140029, 150005, 'richeese nabati 20pcs', 118, 0, 'Makanan ringan', '2018-05-31 00:00:00', 9000, 8000),
(140030, 150005, 'Rhichoco 20pcs', 120, 0, 'Makanan ringan', '2018-05-31 00:00:00', 9000, 8000),
(140031, 150005, 'ovaltine biskuit', 300, 0, 'Makanan ringan', '2018-05-31 00:00:00', 11900, 11000),
(140032, 150005, 'chiki chuba', 500, 0, 'Makanan ringan', '2018-05-31 00:00:00', 1250, 600),
(140033, 150005, 'nabati hanzel 12pcs', 100, 0, 'Makanan ringan', '2018-05-31 00:00:00', 22000, 21000),
(140034, 150005, 'beng beng share it 10x9,5gr', 640, 0, 'Makanan ringan', '2018-05-31 00:00:00', 10000, 8500),
(140035, 150005, 'mie lidi gurih nyoiiiyy', 250, 0, 'Makanan ringan', '2018-05-31 00:00:00', 6400, 4500),
(140036, 150005, 'moring cimol kering', 100, 0, 'Makanan ringan', '2018-05-31 00:00:00', 450, 250),
(140037, 150005, 'Tango waffle 20x8gr', 510, 0, 'Makanan ringan', '2018-05-31 00:00:00', 10000, 8000),
(140038, 150005, 'lays tomato kaleng', 150, 0, 'Makanan ringan', '2018-05-31 00:00:00', 45000, 44000),
(140039, 150005, 'nissin wafer coklat', 100, 0, 'Makanan ringan', '2018-05-31 00:00:00', 21250, 20000),
(140040, 150005, 'macaroni ka-kyu', 100, 0, 'Makanan ringan', '2018-05-31 00:00:00', 16650, 15000),
(140041, 150005, 'cho ovo biskuit', 150, 0, 'Makanan ringan', '2018-05-31 00:00:00', 55000, 54000),
(140042, 150005, 'lemonia 20pcs', 100, 0, 'Makanan ringan', '2018-05-18 08:39:13', 18000, 16500),
(140043, 150005, 'pop mie rasa baso', 1654, 0, 'Makanan ringan', '2018-05-18 08:39:13', 3700, 3000),
(140044, 150005, 'gery salut sweet cheese 110g', 2645, 0, 'Makanan ringan', '2018-05-18 08:39:13', 5999, 5000),
(140045, 150005, 'Slaiolai strawberry 12x24', 145, 0, 'Makanan ringan', '2018-05-18 08:39:13', 10900, 9000),
(140046, 150006, 'apollo milk wafer 12pcs', 670, 0, 'Makanan ringan', '2018-05-18 08:39:13', 8500, 7500),
(140047, 150006, 'good time kaleng', 450, 0, 'Makanan ringan', '2018-05-18 08:39:13', 45000, 44500),
(140048, 150006, 'monde serena 50g', 840, 0, 'Makanan ringan', '2018-05-18 08:39:13', 3800, 3000),
(140049, 150006, 'marie regal 125gr', 17, 0, 'Makanan ringan', '2018-05-18 08:39:13', 8900, 8000),
(140050, 150006, 'rondoreti all variant', 250, 0, 'Makanan ringan', '2018-05-18 08:39:13', 25000, 23000),
(140051, 150006, 'wafer selamat kaleng', 52, 0, 'Makanan ringan', '2018-05-18 08:39:13', 52000, 50000),
(140052, 150004, 'minute maid pulpy orange 1l', 499, 0, 'Minuman', '2018-05-18 08:39:13', 15000, 14500),
(140053, 150006, 'kacang sukro big original', 100, 0, 'Makanan ringan', '2018-05-18 08:39:13', 8500, 8000),
(140054, 150006, 'sosis ayam cikiwiki 375gr', 150, 0, 'Makanan ringan', '2018-05-18 08:39:13', 13900, 12900),
(140055, 150003, 'Kingmix Balado', 150, 0, 'Bumbu Masak', '2018-05-18 08:39:13', 5000, 4000),
(140056, 150007, 'max tea lemon 10pcs', 450, 0, 'Minuman seduh', '2018-05-23 18:00:00', 9700, 8000),
(140057, 150007, 'chocolatos green tea isi4', 450, 0, 'Minuman seduh', '2018-05-23 18:00:00', 7500, 6500),
(140058, 150007, 'kiyora thai tea 1box', 250, 0, 'Minuman seduh', '2018-05-23 18:00:00', 8750, 7500),
(140059, 150007, 'kiyora green tea 1box', 250, 0, 'Minuman seduh', '2018-05-23 18:00:00', 8750, 7500),
(140060, 150007, 'Tong Tji Premium 250 gram', 250, 0, 'Minuman seduh', '2018-05-23 18:00:00', 19500, 18000),
(140061, 150007, 'Sariwangi isi100', 450, 0, 'Minuman seduh', '2018-05-19 15:12:00', 19000, 17000),
(140062, 150007, 'Sariwangi kemasan hemat 25pcs', 450, 0, 'Minuman seduh', '2018-05-19 15:12:00', 5500, 5000),
(140063, 150007, 'Sariwangi teh melati 25pcs', 450, 0, 'Minuman seduh', '2018-05-19 15:12:00', 8420, 6000),
(140064, 150003, 'Gulaku Kuning 1kg', 250, 0, 'Bumbu Masak', '2018-05-19 15:12:00', 12500, 11000),
(140065, 150004, 'Susu Diamond isi6 1l', 310, 0, 'Susu', '2018-05-19 15:12:00', 90500, 85000),
(140066, 150006, 'Milo cube 50pcs', 379, 0, 'Makanan ringan', '2018-05-19 15:12:00', 38500, 38000),
(140067, 150007, 'Milo Profesional 960g', 200, 0, 'Minuman seduh', '2018-05-19 15:12:00', 88000, 87000),
(140068, 150007, 'nestea Thai tea 550gr', 100, 0, 'Minuman seduh', '2018-05-19 15:12:00', 53500, 52000),
(140069, 150004, 'Susu Ultra 1karton 24pcs', 500, 0, 'Susu', '2018-05-19 15:12:00', 107000, 103000),
(140070, 150004, 'Teh botol 250ml', 150, 0, 'Minuman', '2018-05-19 15:12:00', 4800, 4000),
(140071, 150004, 'Royal jelly drink 150ml', 150, 0, 'Minuman', '2018-05-19 15:12:00', 12000, 11500),
(140072, 150007, 'sirup marjan allvariant', 200, 0, 'Minuman seduh', '2018-05-19 15:12:00', 18000, 16500),
(140073, 150007, 'sirup ABC allvariant', 176, 0, 'Minuman seduh', '2018-05-19 15:12:00', 15000, 13500),
(140074, 150003, 'tropicana slim  62,5gr', 500, 0, 'Bumbu Masak', '2018-05-26 18:23:34', 22000, 20000),
(140075, 150003, 'minyak goreng sania 2l', 120, 0, 'Bumbu Masak', '2018-05-26 18:23:34', 23500, 21000),
(140076, 150003, 'minyak goreng filma 2l', 120, 0, 'Bumbu Masak', '2018-05-26 18:23:34', 25000, 24000),
(140077, 150003, 'minyak goreng sunco', 120, 0, 'Bumbu Masak', '2018-05-26 18:23:34', 24000, 23000),
(140078, 150003, 'bimoli spesial 2l', 120, 0, 'Bumbu Masak', '2018-05-26 18:23:34', 29000, 27000),
(140079, 150003, 'bimoli minyak goreng 2l', 120, 0, 'Bumbu Masak', '2018-05-26 18:23:34', 25500, 24500),
(140080, 150003, 'bimoli minyak goreng 6l', 120, 0, 'Bumbu Masak', '2018-05-26 18:23:34', 55000, 52000),
(140081, 150003, 'beras BMW pandan 10kg', 50, 0, 'Bumbu Masak', '2018-05-25 00:00:00', 125000, 123000),
(140082, 150003, 'beras maknyuss 5kg', 50, 0, 'Bumbu Masak', '2018-05-26 18:23:34', 65000, 63000),
(140083, 150003, 'beras hotel 5kg', 50, 0, 'Bumbu Masak', '2018-05-26 18:23:34', 57000, 55000),
(140084, 150003, 'top koki 5kg beras', 50, 0, 'Bumbu Masak', '2018-05-26 18:23:34', 56900, 54000),
(140085, 150003, 'beras bunga 20kg', 50, 0, 'Bumbu Masak', '2018-05-26 18:23:34', 218000, 215000),
(140086, 150002, 'Sabun beras vco', 540, 0, 'Alat mandi', '2018-05-25 13:09:20', 2500, 2000),
(140087, 150003, 'beras rojo lele 10kg', 50, 0, 'Bumbu Masak', '2018-05-26 18:23:34', 120000, 100000),
(140088, 150001, 'seblak mommy', 497, 0, 'Makanan instan', '2018-05-02 11:20:00', 10000, 8500),
(140089, 150001, 'quaker oat meal', 500, 0, 'Makanan instan', '2018-05-02 11:20:00', 12500, 11000),
(140090, 150001, 'pronas sardine tomat', 500, 0, 'Makanan instan', '2018-05-02 11:20:00', 14000, 13500),
(140091, 150001, 'SUN bubur lanjutan', 500, 0, 'Makanan instan', '2018-05-02 11:20:00', 15000, 13500),
(140092, 150001, 'spagetiku cheese 100gr', 500, 0, 'Makanan instan', '2018-05-02 11:20:00', 12500, 11000),
(140093, 150001, 'lafonte spagetti bolognese', 500, 0, 'Makanan instan', '2018-05-02 11:20:00', 7900, 7000),
(140094, 150001, 'la pasta cheese', 500, 0, 'Makanan instan', '2018-05-02 11:20:00', 4950, 4000),
(140095, 150001, 'la fonte spaghetti 225g', 500, 0, 'Makanan instan', '2018-05-02 11:20:00', 12600, 11000),
(140096, 150004, 'coca cola 250ml', 500, 0, 'Minuman', '2018-05-02 11:20:00', 4900, 4000),
(140097, 150004, 'sprite 250ml', 500, 0, 'Minuman', '2018-05-02 11:20:00', 4900, 4000),
(140098, 150004, 'big cola allvariant', 500, 0, 'Minuman', '2018-05-02 11:20:00', 3500, 3000),
(140099, 150002, 'shampoo mane n tail 500gr', 500, 0, 'Alat mandi', '2018-05-02 11:20:00', 125000, 12000),
(140100, 150002, 'vaseline man facial foam 50gr', 250, 0, 'Alat mandi', '2018-05-02 11:20:00', 18500, 18000),
(140101, 150001, 'Indomie all variant', 6842, 0, 'Makanan instan', '2018-05-31 00:00:00', 2300, 1500),
(140102, 150002, 'Sabun lux 4x110gr', 862, 0, 'Alat mandi', '2018-05-19 12:00:00', 15000, 12000),
(140103, 150002, 'shampoo lifebuoy 400ml', 450, 0, 'Alat mandi', '2018-05-19 12:00:00', 21900, 21000),
(140104, 150002, 'gatsby wax', 450, 0, 'Alat mandi', '2018-05-19 12:00:00', 13999, 12000),
(140105, 150002, 'sikat gigi oral B', 1000, 0, 'Alat mandi', '2018-05-19 12:00:00', 10000, 9500),
(140106, 150002, 'pepsodent 225gr', 180, 0, 'Alat mandi', '2018-05-19 12:00:00', 18000, 17500),
(140107, 150002, 'adidas pure game', 450, 0, 'Alat mandi', '2018-05-19 12:00:00', 31999, 30000),
(140108, 150008, 'rinso anti noda', 2645, 0, 'Sabun cuci', '2018-05-19 12:00:00', 30000, 28000),
(140109, 150008, 'wipol karbol ', 500, 0, 'Pembersih lantai & dapur', '2018-05-19 12:00:00', 15500, 14000),
(140110, 150008, 'molto cair 300ml', 2460, 0, 'Sabun cuci', '2018-05-19 12:00:00', 15000, 13500);

-- --------------------------------------------------------

--
-- Table structure for table `product_sale`
--

CREATE TABLE `product_sale` (
  `ps_id` int(6) NOT NULL,
  `pr_id` int(6) DEFAULT NULL,
  `t_id` varchar(20) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `profit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_sale`
--

INSERT INTO `product_sale` (`ps_id`, `pr_id`, `t_id`, `qty`, `price`, `profit`) VALUES
(160093, 140001, '180708232441', 1, 8350, 850),
(160094, 140005, '180708232441', 2, 77998, 4998),
(160095, 140004, '180708232441', 1, 35100, 1600),
(160096, 140001, '180708233249', 1, 8350, 850),
(160097, 140002, '180708233249', 1, 36500, 2300),
(160098, 140003, '180708233350', 2, 48640, 5340),
(160099, 140110, '180708233350', 3, 45000, 4500),
(160100, 140001, '180708233444', 1, 8350, 850),
(160101, 140002, '180708233624', 1, 36500, 2300),
(160102, 140005, '180708233624', 1, 38999, 2499),
(160103, 140001, '180708233829', 1, 8350, 850),
(160104, 140002, '180708233829', 1, 36500, 2300);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `s_id` int(6) NOT NULL,
  `s_nama` varchar(50) NOT NULL,
  `s_email` varchar(100) DEFAULT NULL,
  `s_telp` varchar(12) DEFAULT NULL,
  `s_address` varchar(120) NOT NULL,
  `s_city` varchar(30) NOT NULL,
  `s_province` varchar(30) NOT NULL,
  `s_country` varchar(30) NOT NULL,
  `s_postal_code` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`s_id`, `s_nama`, `s_email`, `s_telp`, `s_address`, `s_city`, `s_province`, `s_country`, `s_postal_code`) VALUES
(150001, 'Snack murah', 'snacksmak@gmail.com', '089516452365', 'rasarasanya kangen dia raya km12', 'Tangerang', 'Banten', 'Indonesia', '15182'),
(150002, 'Unilever', 'Unilever@gmail.com', '0215916326', 'planetluar no12', 'Bekasi', 'Jawa Barat', 'Indonesia', '12645'),
(150003, 'masakmasak', 'masakiya@yahoo.co.id', '02164587953', 'perintis raya no 13', 'Tangerang', 'Banten', 'Indonesia', '18152'),
(150004, 'Minuman empuk', 'empukdiminum@gmail.com', '089516452765', 'empuk empuk no 13', 'Jakarta', 'DKI Jakarta', 'Indonesia', '15126'),
(150005, 'chiki citoz', 'citozdijotoz@gmail.com', '0215915462', 'jotozjotozan no 13', 'Jakarta', 'DKI Jakarta', 'Indonesia', '14652'),
(150006, 'Micin seleraku', 'micinseleraku@gmail.com', '081326459578', 'jauh make helm no 165', 'Kudus', 'Jawa Timur', 'Indonesia', '11223'),
(150007, 'Minumanseret', 'seretdiminum@gmail.com', '0818845597', 'jl.kenagan 14 no 5', 'Tangerang', 'Banten', 'Indonesia', '13132'),
(150008, 'sukanya bersih', 'bersihkusuka@gmail.com', '0215916326', 'sultan agung1 no 16', 'Tangerang', 'Banten', 'Indonesia', '15182');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `t_id` varchar(20) NOT NULL,
  `mem_id` int(6) NOT NULL,
  `total_profit` int(11) NOT NULL,
  `total_price` int(11) NOT NULL,
  `em_id` int(6) DEFAULT NULL,
  `disc` int(11) DEFAULT NULL,
  `date_transaction` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`t_id`, `mem_id`, `total_profit`, `total_price`, `em_id`, `disc`, `date_transaction`) VALUES
('180708232441', 130002, 7076, 115376, 110008, 5, '2018-07-08 23:24:41'),
('180708233249', 130003, 3150, 44850, 110008, 0, '2018-07-08 23:32:49'),
('180708233350', 130019, 8856, 84276, 110008, 10, '2018-07-08 23:33:50'),
('180708233444', 130002, 850, 8350, 110008, 0, '2018-07-08 23:34:44'),
('180708233624', 131111, 4799, 75499, 110008, 0, '2018-07-08 23:36:24'),
('180708233829', 130004, 2993, 42608, 110008, 5, '2018-07-08 23:38:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addstock`
--
ALTER TABLE `addstock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_product_p_id` (`p_id`),
  ADD KEY `fk_employee_em_id` (`em_id`),
  ADD KEY `fk_supplier_s_id` (`s_id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`em_id`);

--
-- Indexes for table `job_position`
--
ALTER TABLE `job_position`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `management_menu`
--
ALTER TABLE `management_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`mem_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`pr_id`),
  ADD KEY `fk_store_s_id` (`s_id`);

--
-- Indexes for table `product_sale`
--
ALTER TABLE `product_sale`
  ADD PRIMARY KEY (`ps_id`),
  ADD KEY `pr_id` (`pr_id`),
  ADD KEY `t_id` (`t_id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`s_id`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`t_id`),
  ADD KEY `fk_member_mem_id` (`mem_id`),
  ADD KEY `em_id` (`em_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addstock`
--
ALTER TABLE `addstock`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=210000;
--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `em_id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110018;
--
-- AUTO_INCREMENT for table `job_position`
--
ALTER TABLE `job_position`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `management_menu`
--
ALTER TABLE `management_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `mem_id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131112;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `pr_id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=140111;
--
-- AUTO_INCREMENT for table `product_sale`
--
ALTER TABLE `product_sale`
  MODIFY `ps_id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=160105;
--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `s_id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=150009;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `product_sale`
--
ALTER TABLE `product_sale`
  ADD CONSTRAINT `product_sale_ibfk_1` FOREIGN KEY (`pr_id`) REFERENCES `product` (`pr_id`),
  ADD CONSTRAINT `product_sale_ibfk_2` FOREIGN KEY (`t_id`) REFERENCES `transaction` (`t_id`);

--
-- Constraints for table `transaction`
--
ALTER TABLE `transaction`
  ADD CONSTRAINT `transaction_ibfk_1` FOREIGN KEY (`em_id`) REFERENCES `employee` (`em_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
